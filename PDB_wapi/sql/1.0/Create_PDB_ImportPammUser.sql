USE Reports
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jack W.
-- Create date: 12/21/2016
-- Description:	Merry Christmas
-- =============================================
CREATE PROCEDURE PDB_ImportPammUser
	@pammID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @newID INT;

    INSERT INTO [dbo].[PDB_Employee]
           ([Employee Name]
           ,[Position]
           ,[Status]
           ,[PammId]
           ,[Active])
	SELECT vcFirstName + ' ' + vcLastName Name, 
		case when vcRole = 't' then 'TL' else 'MA' end as Position,
		'Full Time' [Status],
		biLoginID PammId,
		1 Active
	from pamm.dbo.UserLogin where biLoginID = @pammID;

	SELECT @newID = @@identity;

	INSERT INTO [dbo].[PDB_EmployeeSkillGroups]
           ([PDBId]
           ,[SkillGroupId]
           ,[PhoneLogin])
     SELECT @newID, 1, iACDAgentID
	 FROM pamm.dbo.UserLogin where biLoginID = @pammID;
END
GO
