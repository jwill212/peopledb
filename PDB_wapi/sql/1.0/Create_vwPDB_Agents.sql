USE [Reports]
GO

/****** Object:  View [dbo].[vwPDB_Agents]    Script Date: 3/14/2017 11:02:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vwPDB_Agents]
AS

Select DISTINCT 
e.EmpID, 
e.[Employee Name], 
e.[Start Date], 
e.Position, 
e.[Solo Date], 
e.[Status], 
e.MA, 
e.Resignation, 
e.[Cost Center], 
e.Training, 
e.Supervisor, 
e.Production, 
e.[CP Level], 
e.[Status Info], 
e.Location, 
mgr.[Employee Name] Supervisor1, 
seniorMgr.[Employee Name] as Manager,
e.SKL, 
mgr.[Employee Name] TEAM, 
e.Location1, 
e.PDBId, 
e.PammId, 
e.Active
, SUBSTRING((
	select ',' + PhoneLogin as [text()]
	from PDB_EmployeeSkillGroups
	where PDB_EmployeeSkillGroups.PDBId = e.PDBId
	order by PhoneLogin
	FOR XML PATH('')
),2,1000) [PhoneLogin]
, SUBSTRING((
	select ',' + GroupName as [text()]
	from PDB_EmployeeSkillGroups
	JOIN PDB_SkillGroup on SkillGroupId = ID
	where PDB_EmployeeSkillGroups.PDBId = e.PDBId
	order by GroupName
	FOR XML PATH('')
),2,1000) [SkillGroup],
isnull(x.BusinessLine,'Unknown') as [BusinessLine]
from PDB_Employee e
left join PDB_Manager_Employee m on e.PDBId = m.EmployeeID
left join PDB_Employee mgr on m.ManagerID = mgr.PDBId
left join PDB_Manager_Employee m2 on mgr.PDBId = m2.EmployeeID
left join PDB_Employee seniorMgr on m2.ManagerID = seniorMgr.PDBId
outer APPLY ( 
	SELECT top 1 bl.PDBCodeTitle BusinessLine FROM PDB_EmployeeSkillGroups esg
	JOIN PDB_SkillGroup sg on esg.SkillGroupId = sg.ID
	JOIN PDB_Codes BL on sg.[BusinessLineID] = BL.PDBCodeId
	WHERE esg.PDBId = e.PDBId
) X
where e.[Employee Name] is not null 
AND e.Active = 1





GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 14
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      PaneHidden = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwPDB_Agents'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwPDB_Agents'
GO


