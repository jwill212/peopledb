﻿using PDB_wapi.DAO;
using PDB_wapi.Data;
using PetaPoco;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace PDB_wapi.Controllers
{
    [RoutePrefix("api/Agents")]
    public class AgentsController : PDB_ApiController
    {
        DAOLog<PDB_Employee> _logger;

        public AgentsController() : base()
        {
            _logger = new DAOLog<PDB_Employee>();
        }

        [PDB_Authorized]
        [HttpGet]
        public HttpResponseMessage GetAllAgents(string Filter)
        {
            DAODates dao = new DAODates();
            PDB_Employee[] col;
            col = dao.GetAgents(Filter);

            var response = AdminController.PackageResponseAndSetCache(Request, col);

            return response;
        }

        [PDB_Authorized]
        [HttpGet]
        public HttpResponseMessage GetAgent(int PDBID)
        {
            DAODates dao = new DAODates();
            var col = dao.GetAgent(PDBID);
            var response = AdminController.PackageResponseAndSetCache(Request, col);
            return response;
        }

        [PDB_Authorized]
        [HttpGet]
        [Route("CreateNew")]
        public HttpResponseMessage GetNewAgentID()
        {
            var newAgent = new PDB_Employee();
            DAODates dao = new DAODates();
            dao.InsertAgent(newAgent);
            return Request.CreateResponse(newAgent.PDBId);
        }

        //SAVE
        [PDB_Authorized(PDB_AccessLevels.Edit)]
        [HttpPost]
        public HttpResponseMessage SaveAgent(PDB_Employee ag)
        {
            if(ag == null) { return Request.CreateResponse(HttpStatusCode.NoContent); }

            DAODates dao = new DAODates();
            dao.UpdateAgent(ag);

            TeamDAO dao_team = new TeamDAO();
            dao_team.UpdateUserTeam(ag.PDBId, ag.Supervisor1);

            return Request.CreateResponse<PDB_Employee>(HttpStatusCode.OK, ag);
        }

        [PDB_Authorized(PDB_AccessLevels.Edit)]
        [HttpPost]
        [Route("Term")]
        public HttpResponseMessage Terminate(int Agent, string Date, string Reason, string Comments)
        {
            DAODates dao = new DAODates();
            var oldObj = dao.GetAgent(Agent)[0];
            db.Execute(@"UPDATE PDB_Employee 
SET Resignation = @0, 
[Status Info] = [Status Info] + @1 
WHERE PDBId = @2",
            Date, "  Resignation:" + Reason + " - " + Comments, Agent);
            _logger.LogUpdate(oldObj);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [PDB_Authorized(PDB_AccessLevels.Edit)]
        [HttpPost]
        [Route("LOA")]
        public HttpResponseMessage LOA(int Agent, string Start, string End)
        {
            DAODates dao = new DAODates();
            var oldObj = dao.GetAgent(Agent)[0];
            db.Execute(@"UPDATE PDB_Employee 
SET [Status Info] = [Status Info] + @0 
WHERE PDBId = @1",
            string.Format("  LOA {0} - {1}", Start, End), 
            Agent);

            _logger.LogUpdate(oldObj);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [PDB_Authorized(PDB_AccessLevels.Edit)]
        [HttpDelete]
        public HttpResponseMessage DeleteAgent(int PDBId)
        {
            DAODates dao = new DAODates();
            db.Execute("DELETE FROM [Reports].[dbo].[PDB_TL_Assignments] WHERE AgentID = @0", PDBId);
            db.Execute("DELETE FROM [Reports].[dbo].[PDB_EmployeeSkillGroups] WHERE PDBId = @0", PDBId);
            dao.DeleteAgent(PDBId);

            _logger.LogDelete(PDBId.ToString());
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [PDB_Authorized(PDB_AccessLevels.Edit)]
        [HttpPost]
        [Route("BulkChange")]
        public HttpResponseMessage BulkChange(int ID, string Area, string NewVal)
        {
            DAODates dao = new DAODates();
            if (Area.Equals("TL", System.StringComparison.CurrentCultureIgnoreCase))
            {
                var oldObj = dao.GetAgent(ID)[0];
                var newTLId = int.Parse(NewVal);
                var TL = dao.GetAgent(newTLId)[0];
                if (TL.Location != oldObj.Location)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Can not assign an agent to a team lead in a different location!");
                }
                TeamDAO.UpdateTeamLead(oldObj, newTLId);
                _logger.LogUpdate(oldObj, "Team Lead");
            }
            else if (Area.Equals("pos", System.StringComparison.CurrentCultureIgnoreCase))
            {
                var newObj = dao.GetAgent(ID)[0];
                newObj.Position = NewVal;
                dao.UpdateAgent(newObj);
            }
            else if (Area.Equals("cc", System.StringComparison.CurrentCultureIgnoreCase))
            {
                var newObj = dao.GetAgent(ID)[0];
                newObj.CostCenter = NewVal;
                dao.UpdateAgent(newObj);
            }
            else if (Area.Equals("sg", System.StringComparison.CurrentCultureIgnoreCase))
            {
                var oldObj = dao.GetAgent(ID)[0];
                SkillsController ctrler = new SkillsController();
                ctrler.ChangeAgentGroup(ID, int.Parse(NewVal));
                _logger.LogUpdate(oldObj, "Skill Group");
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
}
