﻿using PDB_wapi.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PDB_wapi.Controllers
{
    [RoutePrefix("api/Codes")]
    public class CodesController : PDB_ApiController
    {
        DAOLog<PDB_Codes> _logger;
        public CodesController() : base()
        {
            _logger = new DAOLog<PDB_Codes>();
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpGet]
        public HttpResponseMessage GetCodeTypes()
        {
            var results = db.Fetch<string>("SELECT distinct PDBCodeType FROM PDB_Codes");
            return Request.CreateResponse(HttpStatusCode.OK, results);
        }

        [PDB_Authorized(PDB_AccessLevels.Edit)]
        [HttpGet]
        [Route("{Type}")]
        public HttpResponseMessage GetCodes(string Type)
        {
            var results = db.Fetch<PDB_Codes>("SELECT * FROM PDB_Codes where PDBCodeType = @0 order by PDBCodeTitle, PDBCodeAbrv", Type);
            return Request.CreateResponse(HttpStatusCode.OK, results);
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpPost]
        public HttpResponseMessage AddCode(string group, string title, string abbrev)
        {
            PDB_Codes newCode = new PDB_Codes() { Type = group, Title = title, Abbrev = abbrev };
            var newID = db.Insert(newCode);
            _logger.LogInsert(newID.ToString());
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpDelete]
        [Route("{id}")]
        public HttpResponseMessage DeleteCode(int id)
        {
            db.Delete(new PDB_Codes() { ID = id });
            _logger.LogDelete(id.ToString());
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}