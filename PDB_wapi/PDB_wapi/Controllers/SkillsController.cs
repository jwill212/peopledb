﻿using PDB_wapi.Data;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PDB_wapi.Controllers
{
    [RoutePrefix("api/Skills")]
    public class SkillsController : PDB_ApiController
    {
        DAOLog<SkillsController> _logger;
        public SkillsController() : base()
        {
            _logger = new DAOLog<SkillsController>();
        }

        [PDB_Authorized]
        [HttpGet]
        public Skill[] GetSkills(string businessLine)
        {
            var retVal = db.Fetch<Skill>("select Skill as [value], SkillName as [label] from KPI_BusinessSkills where BusinessLine = 'Admin' OR BusinessLine = @0", businessLine);
            return retVal.ToArray();
        }

        [PDB_Authorized]
        [HttpGet]
        public SkillGroupSkill[] SkillGroupList(int groupID)
        {
            var retVal = db.Fetch<SkillGroupSkill>(
@"SELECT ID, BL.PDBCodeTitle BusinessLine, GroupName, BS.Skill as SkillID, SkillName, [Priority] 
FROM [dbo].[PDB_SkillGroup] SG
JOIN PDB_Codes BL on SG.[BusinessLineID] = BL.[PDBCodeId]
LEFT JOIN [dbo].[PDB_SkillGroupSkills] SGS ON SG.ID = SGS.[SkillGroupID]
LEFT JOIN [dbo].[KPI_BusinessSkills] BS ON SGS.[BusinessSkillId] = BS.Skill
WHERE SG.ID = @0
ORDER BY GroupName", groupID);
            return retVal.ToArray();
        }

        [PDB_Authorized]
        [HttpGet]
        public SkillGroupTableRow[] SkillGroupSummary()
        {
            var retVal = db.Fetch<SkillGroupTableRow>(
@"SELECT ID, PDBCodeTitle BusinessLine, GroupName, count(SGS.SkillGroupID) AS SkillCount
FROM [dbo].[PDB_SkillGroup] SG
JOIN PDB_Codes BL ON SG.[BusinessLineID] = BL.[PDBCodeId]
LEFT JOIN [dbo].[PDB_SkillGroupSkills] SGS ON SG.ID = SGS.SkillGroupID
GROUP BY ID, PDBCodeTitle, GroupName
ORDER BY GroupName");
            return retVal.ToArray();
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpPost]
        public HttpResponseMessage SaveSkill(SkillGroupSkill[] newSkills)
        {
            if (newSkills.Length > 0)
            {
                try
                {
                    int groupID = 0;

                    db.BeginTransaction();
                    if (newSkills[0].ID == 0)
                    {
                        var sqlInsertGroup = new Sql(
    String.Format(@"DECLARE @@blID int;
SELECT @@blID = PDBCodeId FROM [dbo].[PDB_Codes] WHERE PDBCodeTitle = '{0}';
INSERT INTO [PDB_SkillGroup] (GroupName, BusinessLineID) VALUES ('{1}',@@blID); 
SELECT @@@IDENTITY as NewID;"
        , newSkills[0].BusinessLine, newSkills[0].GroupName));
                        groupID = db.ExecuteScalar<int>(sqlInsertGroup);
                    }else
                    {
                        groupID = newSkills[0].ID;
                    }

                    foreach(SkillGroupSkill skill in newSkills)
                    {
                        if (skill.Action == "Add")
                        {
                            db.Execute(@"INSERT INTO [dbo].[PDB_SkillGroupSkills] (BusinessSkillId, SkillGroupID, [Priority]) VALUES (@0,@1,@2)",
                                skill.SkillID,
                                groupID,
                                skill.Priority);
                            _logger.LogInsert(skill.SkillID.ToString() + "," + groupID.ToString());
                        }
                        else if(skill.Action == "Delete")
                        {
                            db.Execute(@"DELETE FROM [dbo].[PDB_SkillGroupSkills] WHERE SkillGroupID = @0 AND BusinessSkillId = @1", groupID, skill.SkillID);
                            _logger.LogDelete(skill.SkillID.ToString() + "," + groupID.ToString());
                        }
                    }

                    db.CompleteTransaction();

                    return Request.CreateResponse(HttpStatusCode.Accepted);
                }
                catch(Exception ex)
                {
                    db.AbortTransaction();
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "The list of skills to add was empty");
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpDelete]
        public HttpResponseMessage Delete(int groupID)
        {
            db.Execute("DELETE FROM [dbo].[PDB_EmployeeSkillGroups] WHERE SkillGroupId = @0", groupID);
            var sql = new Sql("DELETE FROM PDB_SkillGroup WHERE ID = @0; DELETE FROM PDB_SkillGroupSkills WHERE SkillGroupID = @0;", groupID);
            db.Execute(sql);
            _logger.LogDelete(groupID.ToString());
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpPost]
        public HttpResponseMessage AddAgentGroup(int AgentID, int SkillGroupID, string PhoneLogin)
        {
            if(db.ExecuteScalar<int>("SELECT count(*) FROM PDB_EmployeeSkillGroups WHERE PhoneLogin = @0",PhoneLogin) > 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "The phone login specified is already in use.");
            }
            var sql = new Sql("INSERT INTO [dbo].[PDB_EmployeeSkillGroups] ([PDBId],[SkillGroupId],[PhoneLogin]) VALUES (@0,@1,@2)", AgentID, SkillGroupID, PhoneLogin);
            db.Execute(sql);
            _logger.LogInsert(String.Format("{0},{1},{2}", AgentID, SkillGroupID, PhoneLogin));
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [PDB_Authorized]
        [HttpGet]
        [Route("AgentGroups/{AgentID}")]
        public HttpResponseMessage GetAgentGroups(int AgentID)
        {
            var sql = new Sql(@"SELECT esg.SkillGroupId, esg.PhoneLogin, sg.GroupName + ' (' + bl.PDBCodeTitle + ')' GroupName
from PDB_EmployeeSkillGroups esg
join PDB_SkillGroup sg on esg.SkillGroupId = sg.ID
join PDB_Codes bl on sg.BusinessLineID = bl.PDBCodeId
where esg.PDBId = @0", AgentID);
            var results = db.Fetch<AgentSkillGroups>(sql);
            return Request.CreateResponse(HttpStatusCode.OK,results.ToArray());
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpDelete]
        public HttpResponseMessage DeleteAgentGroup(int AgentID, int SkillGroupID, string PhoneLogin)
        {
            var sql = new Sql("DELETE FROM [dbo].[PDB_EmployeeSkillGroups]");
            sql.Where("[PDBId] = @0 AND[SkillGroupId] = @1", AgentID, SkillGroupID);

            if (PhoneLogin == null) { sql.Where("PhoneLogin IS NULL"); }
            else { sql.Where("PhoneLogin = @0", PhoneLogin); }

            db.Execute(sql);
            _logger.LogDelete(String.Format("{0},{1},{2}", AgentID, SkillGroupID, PhoneLogin));
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [PDB_Authorized]
        [HttpGet]
        [Route("AgentList/{groupID}")]
        public HttpResponseMessage GetAgentList(int groupID)
        {
            var sql = new Sql(@"SELECT PhoneLogin from [dbo].[PDB_EmployeeSkillGroups] 
where SkillGroupId = @0", groupID);
            var results = db.Fetch<int>(sql);
            //Group into blocks of 49
            List<string> retval = new List<string>();
            string curList = "";
            for (int index = 0; index < results.Count; index++)
            {
                if (index % 49 == 0 && curList.Length > 0)
                {
                    retval.Add(curList.TrimEnd(','));
                    curList = "";
                }
                curList += results[index].ToString() + ",";
            }
            if(curList.Length > 0) { retval.Add(curList.TrimEnd(',')); }

            return Request.CreateResponse(HttpStatusCode.OK,retval.ToArray());
        }

        internal void ChangeAgentGroup(int agentID, int skillGroup)
        {
            var sql = new Sql(@"UPDATE PDB_EmployeeSkillGroups SET SkillGroupID = @1 WHERE PDBId = @0; SELECT @@@@ROWCOUNT;", agentID, skillGroup);
            db.BeginTransaction();
            int numRows = db.ExecuteScalar<int>(sql);
            if(numRows > 1) { db.AbortTransaction(); }
            db.CompleteTransaction();

            if(numRows == 0)
            {
                db.Execute("INSERT INTO [dbo].[PDB_EmployeeSkillGroups] ([PDBId],[SkillGroupId]) VALUES (@0, @1)", agentID, skillGroup);
            }
        }
    }
}
