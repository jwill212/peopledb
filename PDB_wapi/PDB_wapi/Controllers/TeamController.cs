﻿using PDB_wapi.Data;
using PetaPoco;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PDB_wapi.Controllers
{

    [RoutePrefix("api/Team")]
    public class TeamController : PDB_ApiController
    {
        DAOLog<TeamController> _logger;
        public TeamController() : base()
        {
            _logger = new DAOLog<TeamController>();
        }

        /// <summary>
        /// Get a list of all team leads for a particular call center location
        /// </summary>
        /// <param name="location">The airport abbreviation for the call center location (DFW, GSP,...)</param>
        /// <returns>A list of team lead names and PDBIDs</returns>
        /// <remarks>Access:Anyone</remarks>
        [PDB_Authorized]
        [HttpGet]
        public HttpResponseMessage GetTeamLeads(string location)
        {
            var results = DAO.TeamDAO.GetTeamLeads(location);
            var response = AdminController.PackageResponseAndSetCache(Request, results);
            return response;
        }
        
    }
}