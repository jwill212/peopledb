﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;

namespace KPI_wapi.Controllers
{
   
    public class PeopleController : ApiController
    {
       
        [HttpGet]
        public HttpResponseMessage Get(string Location, string BusinessLine, string Manager, string TeamLead, string Agent, string StartDate, string EndDate,string NameType)
        {
            Agent = Common.RemoveHyphen(Agent);
            DAOPeople_Mongo dsoPeople = new DAOPeople_Mongo();

            if(Location == null)
            { Location = "All";}
            if (BusinessLine == null)
            { BusinessLine = "All"; }
            if (Manager == null)
            { Manager = "All"; }
            if (TeamLead == null)
            { TeamLead = "All"; }
            if (Agent == null)
            { Agent = "All"; }

            Location = Location.Trim();
            BusinessLine = BusinessLine.Trim();
            Manager = Manager.Trim();
            TeamLead = TeamLead.Trim();
            Agent = Agent.Trim();
            NameType = NameType.Trim();

            List<Person> aCol = dsoPeople.GetPeoplList(Location, BusinessLine, Manager, TeamLead, Agent, StartDate, EndDate, NameType);


            HttpStatusCode statusMsg = new HttpStatusCode();
            if (aCol == null)
            {
                statusMsg = HttpStatusCode.NotFound;
            }
            else
            {
                statusMsg = HttpStatusCode.OK;
            }
            var response = Request.CreateResponse(statusMsg, aCol);
            response.Headers.ConnectionClose = true;
            response.Headers.CacheControl = new CacheControlHeaderValue();
            response.Headers.CacheControl.Public = true;

            return response;
        }
        [HttpGet]
        public HttpResponseMessage GetCurrent(string NameType)
        {

            NameType = NameType.Trim();


            DAOPeople_Mongo dsoPeople = new DAOPeople_Mongo();

            List<Person> aCol = dsoPeople.GetCurrentPeoplList(NameType);


            HttpStatusCode statusMsg = new HttpStatusCode();
            if (aCol == null)
            {
                statusMsg = HttpStatusCode.NotFound;
            }
            else
            {
                statusMsg = HttpStatusCode.OK;
            }
            var response = Request.CreateResponse(statusMsg, aCol);
            response.Headers.ConnectionClose = true;
            response.Headers.CacheControl = new CacheControlHeaderValue();
            response.Headers.CacheControl.Public = true;

            return response;
        }
        [HttpGet]
        public HttpResponseMessage GetBusinessLine(string StartDate, string EndDate,string Name, string NameType)
        {
            Name = Name.Trim();
            Name = Common.RemoveHyphen(Name);
            NameType = NameType.Trim();
            int PammId = CookieHandler.GetPammIdFromCookie(Request);
          
            DAOPeople_Mongo dsoPeople = new DAOPeople_Mongo();
         
            List<Person> aCol = dsoPeople.GetBusinessLine(StartDate, EndDate, Name, NameType, PammId);


            HttpStatusCode statusMsg = new HttpStatusCode();
            if (aCol == null)
            {
                statusMsg = HttpStatusCode.NotFound;
            }
            else
            {
                statusMsg = HttpStatusCode.OK;
            }
            var response = Request.CreateResponse(statusMsg, aCol);
            response.Headers.ConnectionClose = true;
            response.Headers.CacheControl = new CacheControlHeaderValue();
            response.Headers.CacheControl.Public = true;

            return response;
        }
      
    }
}
