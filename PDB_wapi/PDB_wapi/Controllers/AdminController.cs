﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using PDB_wapi.Data;

namespace PDB_wapi.Controllers
{
    [RoutePrefix("api/Admin")]
    public class AdminController : PDB_ApiController
    {
        public const string ALL_VALUE = "All";


        [PDB_Authorized]
        [HttpGet]
        [Route("CurrentUser")]
        public HttpResponseMessage GetCurrentUser()
        {
            var userName = ((ReportSubscriberPrincipal)System.Threading.Thread.CurrentPrincipal).UserName;
            return Request.CreateResponse(HttpStatusCode.OK, userName);
        }

        [PDB_Authorized]
        [HttpGet]
        [Route("Version")]
        public HttpResponseMessage GetVersion()
        {
            return Request.CreateResponse(HttpStatusCode.OK, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpGet]
        [Route("SearchPAMM")]
        public HttpResponseMessage SearchPamm(string search)
        {
            var results = db.Fetch<PammUser>(@"SELECT biLoginID ID, vcFirstName + ' ' + vcLastName Name, vcRole [Role] 
from pamm.dbo.UserLogin where bIsRequired = 1 AND vcRole not in ('M','C') AND
(vcFirstName like @0 OR 
vcLastName like @0)", "%" + search + "%");
            return Request.CreateResponse(HttpStatusCode.OK, results);
        }

        [PDB_Authorized(PDB_AccessLevels.Admin)]
        [HttpPost]
        [Route("ImportPammUser/{id}")]
        public HttpResponseMessage ImportPammUser(int id)
        {
            db.Execute(@"EXEC PDB_ImportPammUser @0", id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public static HttpResponseMessage PackageResponseAndSetCache(HttpRequestMessage Request, object aCol)
        {
            HttpStatusCode statusMsg = new HttpStatusCode();
            if (aCol == null)
            {
                statusMsg = HttpStatusCode.NotFound;
            }
            else
            {
                statusMsg = HttpStatusCode.OK;
            }
            var response = Request.CreateResponse(statusMsg, aCol);
            response.Headers.ConnectionClose = true;
            response.Headers.CacheControl = new CacheControlHeaderValue();
            response.Headers.CacheControl.Public = true;
            return response;
        }
    }
}
