﻿using PDB_wapi.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PDB_wapi.Controllers
{
    [RoutePrefix("api/Stats")]
    public class StatsController : PDB_ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [PDB_Authorized]
        [HttpGet]
        public HttpResponseMessage GetCurrentHeadCount()
        {
            string agentOnlyFilter = DAODates.Replace("agentonly", "", null);
            string activeFilter = DAODates.Replace("active", "", null);

            var results = db.Fetch<StatisticsResults>(string.Format(@"SELECT 
	e.BusinessLine as [grouping],
	x.PDBCodeTitle location, 
	sum(case when x.PDBCodeAbrv = e.Location then 1 else 0 end) as [count]
from [dbo].[vwPDB_Agents] e
CROSS APPLY (Select c_loc.PDBCodeTitle, c_loc.PDBCodeAbrv from PDB_Codes c_loc where c_loc.PDBCodeType = 'LOC') x
WHERE {1} AND {0}
GROUP BY x.PDBCodeTitle, e.BusinessLine 
UNION
SELECT 
	'Total' as [grouping],
	x.PDBCodeTitle location, 
	sum(case when x.PDBCodeAbrv = e.Location then 1 else 0 end) as [count]
from [dbo].[vwPDB_Agents] e
CROSS APPLY (Select c_loc.PDBCodeTitle, c_loc.PDBCodeAbrv from PDB_Codes c_loc where c_loc.PDBCodeType = 'LOC') x
WHERE {1} AND {0}
GROUP BY x.PDBCodeTitle
ORDER BY [grouping], location", agentOnlyFilter, activeFilter));
            var retVal = StatisticsResultObj.CreateFromQueryResults(results);
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        [PDB_Authorized]
        [HttpGet]
        [Route("Daily")]
        public HttpResponseMessage GetDailyHeadCount()
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented);
        }

        [PDB_Authorized]
        [HttpGet]
        [Route("Weekly")]
        public HttpResponseMessage GetWeeklyHeadCount()
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented);
        }
    }
}