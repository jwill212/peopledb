﻿using PetaPoco;
using System.Web.Http;

namespace PDB_wapi.Controllers
{
    public class PDB_ApiController : ApiController
    {
        protected readonly Database db;

        public PDB_ApiController() :base()
        {
            db = new Database("primaryDBConn");
        }
    }
}