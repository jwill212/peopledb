﻿using System;
using PetaPoco;
using PDB_wapi.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace PDB_wapi
{
    public class DAODates
    {
        Database db = null;
        ReportSubscriberPrincipal _user;
        DAOLog<PDB_Employee> _logger;

        public DAODates()
        {
            db = new Database("primaryDBConn");
            _user = (ReportSubscriberPrincipal)System.Threading.Thread.CurrentPrincipal;
            _logger = new DAOLog<PDB_Employee>();
        }
        
        internal PDB_Employee[] GetAgents(string PersonType, string Filter)
        {
            return GetAgents(String.Format("{0}:{1}", PersonType, Filter));
        }

        internal PDB_Employee[] GetAgents(string filter)
        {
            if (!filter.Contains(":")) { filter = "agent:" + filter; } //default it to an agent name filter if none is specified

            Sql query = new Sql("SELECT * FROM vwPDB_Agents");

            filter = Regex.Replace(filter, " and ", "&&", RegexOptions.IgnoreCase);
            var match = @"(?<measure>\w+):\s?(?<value>[\w\s\.]*)";
            List<string> args = new List<string>();
            while (Regex.IsMatch(filter.Trim(), match))
            {
                filter = Regex.Replace(filter, match, (x) => { return Replace(x.Groups["measure"].Value, x.Groups["value"].Value, args); });
            }
            filter = Regex.Replace(filter, "&&", " AND ");

            query.Where(filter, args.ToArray());
            query.OrderBy("[Employee Name]");

            var results = db.Fetch<PDB_Employee>(query);

            return results.ToArray();
        }

        internal static string Replace(string measure, string value, List<string> args)
        {
            var argIndex = args == null?0:args.Count;
            switch (measure.ToLower())
            {
                case "active":
                    return "Resignation is null";
                case "loa":
                    return "[Status Info] like '%LOA%' and Resignation is null";
                case "terminated":
                    return "Resignation is not null";
                case "agentonly":
                    return "[Position] NOT IN ('QA','OPM','TL','TAC','TLA')";
                case "id":
                    args.Add("%" + value + "%");
                    return string.Format("PhoneLogin LIKE @{0}", argIndex);
                case "agent":
                    args.Add("%" + value + "%");
                    return string.Format("[Employee Name] LIKE @{0}", argIndex);
                case "team":
                    args.Add("%" + value + "%");
                    return string.Format("[Supervisor1] LIKE @{0}", argIndex);
                case "position":
                    args.Add(value);
                    return string.Format("[Position] = @{0}", argIndex);
                case "location":
                    args.Add(value);
                    return string.Format("([Location] = @{0} OR [Location1] = @{0})", argIndex);
                case "cost":
                    args.Add(value);
                    return string.Format("[Cost Center] = @{0}", argIndex);
                case "skillgroup":
                    args.Add("%" + value + "%");
                    return string.Format("[SkillGroup] LIKE @{0}", argIndex);
                case "businessline":
                    args.Add(value);
                    return string.Format("[BusinessLine] = @{0}", argIndex);
                default:
                    return "";
            }
        }

        internal PDB_Employee[] GetAgent(int PDBID)
        {
            string mySql = @"Select 
e.EmpID, 
e.[Employee Name], 
e.[Start Date], 
e.Position, 
e.[Solo Date], 
e.[Status], 
e.MA, 
e.Resignation, 
e.[Cost Center], 
e.Training, 
eMgr.[Employee Name] [Supervisor], 
e.Production, 
e.[CP Level], 
e.[Status Info], 
e.Location, 
eMgr.PDBId [Supervisor1], 
e.SKL, 
e.TEAM, 
e.Location1, 
e.PDBId, 
e.SkillGroup, 
e.PammId, 
e.Active
from PDB_Employee e
left join PDB_Manager_Employee me on e.PDBId = me.EmployeeID
left join PDB_Employee eMgr on me.ManagerID = eMgr.PDBId
where e.PDBId = @0 and e.Active=1";
            var results = db.Fetch<PDB_Employee>(mySql, PDBID);
            return results.ToArray();
        }

        internal void InsertAgent(PDB_Employee ag)
        {            
            var newID = db.Insert(ag).ToString();
            _logger.LogInsert(newID);
        }

        internal void UpdateAgent(PDB_Employee ag)
        {
            var oldObj = GetAgent(ag.PDBId)[0];
            db.Update(ag);
            //clean up missing location values
            db.Execute(@"update PDB_Employee set Location1 = c.PDBCodeTitle
from PDB_Employee
join PDB_Codes c on Location = c.PDBCodeAbrv
where c.PDBCodeType = 'loc' and Location1 is null");
            _logger.LogUpdate(oldObj);
        }

        internal void DeleteAgent(int id)
        {
            db.Update(new PDB_Employee() {PDBId=id, Active=false}, new[] { "Active" });
            _logger.LogDelete(id.ToString());
        }
    }
}