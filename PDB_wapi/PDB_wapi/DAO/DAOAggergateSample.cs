﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;

using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver.Wrappers;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace availandpricing_externalfeed_handler
{
    class MongoItDAL
    {
       

        #region Properties
        MongoClient client;
        MongoServer server;
        MongoDatabase PricingDatabase;
        MongoCollection<BsonExternalPricing> AvaillevelOneAPP;

        const string Database = "ExternalAvailAndPricing";
        const string ColAvailability = "Availability";

        private string MongoConnectionString; // = System.Configuration.ConfigurationSettings.AppSettings["mongoDBConn"].ToString();
        #endregion

        #region Constructors
        public MongoItDAL()
        {

            MongoConnectionString = System.Configuration.ConfigurationSettings.AppSettings["mongoDBConn"].ToString();
            /*
            MongoClientSettings settings = new MongoClientSettings();
            settings.WaitQueueSize = int.MaxValue;
            settings.WaitQueueTimeout = new TimeSpan(0, 2, 0);
            settings.MinConnectionPoolSize = 1;
            settings.MaxConnectionPoolSize = 25;
            settings.Server = new MongoServerAddress(conn);
            client = new MongoClient(settings);
            //PricingDatabase = client.
            //PricingDatabase = client.GetDatabase(Database);

            AvaillevelOneAPP = PricingDatabase.GetCollection<BsonExternalPricing>(ColAvailability);
            */
        }
        #endregion

        public ObjectId getMostRecentObjectIdForProperty(int propertyId)
        {
            ObjectId recordid = new ObjectId();
            try
            {


                MongoClient client = new MongoClient(MongoConnectionString);
                MongoServer server = client.GetServer();
                MongoDatabase db = server.GetDatabase(Database);
                MongoCollection collection = db.GetCollection<BsonDocument>(ColAvailability);
                var query = Query.EQ("Property.PropertyID", propertyId);

                var sortBy = SortBy.Descending("InsertedDate");

                var result = collection.FindAs<BsonExternalPricing>(query).SetSortOrder(sortBy);

                BsonExternalPricing document = result.FirstOrDefault();

                if (document != null)
                {
                    recordid = document._id;

                }


            }
            catch (Exception ex)
            {
                //handle it later
            }


            return recordid;
        }

        #region Get
        public BsonExternalPricing getAvailableUnitsCache(int propertyID)
        {
            BsonExternalPricing result = null;
            try
            {
                var query = Query.EQ("Property.PropertyID", propertyID);
                var sortBy = SortBy.Descending("InsertedDate");
                //Query.And(
                // Query.EQ("propertyID", propertyID)//,
                // Query.EQ("InsertedDate", DateTime.Now.ToShortDateString())
                // );
                //var resultsCursor = AvaillevelOneAPP.Find<BsonExternalPricing>(query).SetSortOrder(sortBy).SetLimit(1);


                //result = resultsCursor.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting data from cache: " + ex.Message);
            }
            return result;
        }


        public LevelOneAPProperty.Property AggegateByBedBathForPropertyId(int propertyId, ObjectId recordid)
        {
            List<FloorplanAggregate> floorplansFound = new List<FloorplanAggregate>();
            List<LevelOneAPProperty.FloorPlanByBedBath> FloorPlanByBedBath = new List<LevelOneAPProperty.FloorPlanByBedBath>();
            LevelOneAPProperty.Property property = new LevelOneAPProperty.Property();


            #region pipeline
            var unwind = new BsonDocument
            {
                 { 
                        "$unwind", "$Property.Floorplans"
                  }
            };

            var match = new BsonDocument 
                { 
                    { 
                        "$match", 
                        new BsonDocument 
                            { 
                                {"_id", recordid},
                                {"Property.PropertyID", propertyId},
                                {"Property.Floorplans.MarketRentMin", new BsonDocument{{"$gt", 0 }}},
                                {"Property.Floorplans.MarketRentMax", new BsonDocument {{"$gt", 0 } }}
                             }
                      }
                };


            var group = new BsonDocument 
                { 
                    { 
                        "$group", 
                        new BsonDocument 
                            { 
                                {"_id", "$Property.Floorplans.Bed"},
                                {"Bed", new BsonDocument{{"$min","$Property.Floorplans.Bed"}}},
                                {"Bath", new BsonDocument{{"$min","$Property.Floorplans.Bath"}}},
                                {"AvailableNow", new BsonDocument{{"$sum","$Property.Floorplans.AvailableNow"}}},
                                {"Available30", new BsonDocument{{"$sum","$Property.Floorplans.Available30"}}},
                                {"Available60", new BsonDocument{{"$sum","$Property.Floorplans.Available60"}}},
                                {"SquareFeetMin", new BsonDocument{{"$min","$Property.Floorplans.SquareFeetMin"}}},
                                {"SquareFeetMax", new BsonDocument{{"$max","$Property.Floorplans.SquareFeetMax"}}},
                                {"MarketRentMin", new BsonDocument{{"$min","$Property.Floorplans.MarketRentMin"}}},
                                {"MarketRentMax", new BsonDocument{{"$max","$Property.Floorplans.MarketRentMax"}}}
                                
                             }
                      }
                };

            var projection = new BsonDocument 
                { 
                    { 
                        "$project", 
                        new BsonDocument 
                            { 
                                {"_id", 0},
                                {"Floorplan", "$_id"},
                                {"Bed", "$Bed"},
                                {"Bath", "$Bath"},
                                {"AvailableNow", "$AvailableNow"},
                                {"Available30", "$Available30"},
                                {"Available60", "$Available60"},
                                {"SquareFeetMin", "$SquareFeetMin"},
                                {"SquareFeetMax", "$SquareFeetMax"},
                                {"MarketRentMin", "$MarketRentMin"},
                                {"MarketRentMax", "$MarketRentMax"}
                                 
                             }
                      }
                };

            var sort = new BsonDocument { 
            {
                    "$sort", new BsonDocument{{"Bed", 1}}	
                    }
            
            };

            #endregion

            //create the pipeline
            var pipeline = new[] { unwind, match, group, projection, sort };

            //MongoCollection<FloorplanAggregate> AggregatedFloors = PricingDatabase.GetCollection<FloorplanAggregate>(ColAvailability);

            //var result = AggregatedFloors.Aggregate()
            //string conn = System.Configuration.ConfigurationSettings.AppSettings["mongoDBConn"].ToString();
            try
            {


                MongoClient client = new MongoClient(MongoConnectionString);
                MongoServer server = client.GetServer();
                MongoDatabase db = server.GetDatabase(Database);
                MongoCollection collection = db.GetCollection(ColAvailability);
                AggregateArgs args = new AggregateArgs();
                args.Pipeline = pipeline;
                IEnumerable<BsonDocument> aggregates = collection.Aggregate(args);

                if ((aggregates != null) && (aggregates.Count<BsonDocument>() != 0))
                {
                    foreach (var fp in aggregates)
                    {

                        FloorplanAggregate floor = Newtonsoft.Json.JsonConvert.DeserializeObject<FloorplanAggregate>(fp.ToString());
                        //LevelOneAPProperty.Floorplan floor = Newtonsoft.Json.JsonConvert.DeserializeObject<LevelOneAPProperty.Floorplan>(fp.ToString());
                        //floorplansFound.Add(floor);

                        LevelOneAPProperty.FloorPlanByBedBath fpB = new LevelOneAPProperty.FloorPlanByBedBath();

                        fpB.Bed = floor.Bed;
                        fpB.Bath = floor.Bath;
                        fpB.AvailableNow = floor.AvailableNow;
                        fpB.Available30 = floor.Available30;
                        fpB.Available60 = floor.Available60;
                        fpB.SquareFeetMin = floor.SquareFeetMin;
                        fpB.SquareFeetMax = floor.SquareFeetMax;
                        fpB.RentMin = floor.MarketRentMin;
                        fpB.RentMax = floor.MarketRentMax;
                        FloorPlanByBedBath.Add(fpB);

                    }
                }
            }
            catch (Exception e)
            {
                // not sure how to handle this just yet.
            }
            //probably need to move this to floorplansbedandbath aggregate

            property.FloorPlansByBedBath = FloorPlanByBedBath;
            property.PropertyID = propertyId;

            return property;


        }

        public LevelOneAPProperty.Property FloorplansByBedAndBath(int propertyId, ObjectId recordid, double bed, double bath)
        {
            List<FloorplanAggregate> floorplansFound = new List<FloorplanAggregate>();
            List<LevelOneAPProperty.Floorplan> FloorPlanByBedBath = new List<LevelOneAPProperty.Floorplan>();
            LevelOneAPProperty.Property property = new LevelOneAPProperty.Property();

            #region pipeline

            var unwind_Floorplans = new BsonDocument
            {
                  { 
                        "$unwind", "$Property.Floorplans"
                  }
            };

            var unwind_Units = new BsonDocument
            {

                  { 
                        "$unwind", "$Property.Units"
                  }
            };

            var unwind_LeaseTerms = new BsonDocument
            {
                  
                  {
                        "$unwind", "$Property.Units.LeaseTerms"
                  }
            };


            var match = new BsonDocument 
                { 
                    { 
                        "$match", 
                        new BsonDocument 
                            { 
                                {"_id", recordid},
                                {"Property.PropertyID", propertyId},
                                {"Property.Floorplans.Bed", bed},
                                {"Property.Floorplans.Bath", bath},
                                {"Property.Units.Bed", bed},
                                {"Property.Units.Bath", bath},
                                {"Property.Floorplans.MarketRentMin", new BsonDocument{{"$gt", 0 }}},
                                {"Property.Floorplans.MarketRentMax", new BsonDocument {{"$gt", 0 } }}
                             }
                      }
                };

            var group = new BsonDocument 
                { 
                    { 
                        "$group", 
                        new BsonDocument 
                            { 
                                {"_id", "$Property.Floorplans.Name"},
                                {"Bed", new BsonDocument{{"$min","$Property.Floorplans.Bed"}}},
                                {"Bath", new BsonDocument{{"$min","$Property.Floorplans.Bath"}}},
                                {"AvailableNow", new BsonDocument{{"$sum","$Property.Floorplans.AvailableNow"}}},
                                {"Available30", new BsonDocument{{"$sum","$Property.Floorplans.Available30"}}},
                                {"Available60", new BsonDocument{{"$sum","$Property.Floorplans.Available60"}}},
                                {"SquareFeetMin", new BsonDocument{{"$min","$Property.Floorplans.SquareFeetMin"}}},
                                {"SquareFeetMax", new BsonDocument{{"$max","$Property.Floorplans.SquareFeetMax"}}},
                                {"MarketRentMin", new BsonDocument{{"$min","$Property.Units.LeaseTerms.MarketRent"}}},
                                {"MarketRentMax", new BsonDocument{{"$max","$Property.Units.LeaseTerms.MarketRent"}}}
                                
                             }
                      }
                };

            var projection = new BsonDocument 
                { 
                    { 
                        "$project", 
                        new BsonDocument 
                            { 
                                {"_id", 0},
                                {"Floorplan", "$_id"},
                                {"Bed", "$Bed"},
                                {"Bath", "$Bath"},
                                {"AvailableNow", "$AvailableNow"},
                                {"Available30", "$Available30"},
                                {"Available60", "$Available60"},
                                {"SquareFeetMin", "$SquareFeetMin"},
                                {"SquareFeetMax", "$SquareFeetMax"},
                                {"MarketRentMin", "$MarketRentMin"},
                                {"MarketRentMax", "$MarketRentMax"}
                                 
                             }
                      }
                };

            var sort = new BsonDocument { 
            {
                    "$sort", new BsonDocument{{"Floorplan", 1}, {"Bed", 1}}	
                    }
            
            };

            #endregion

            var pipeline = new[] { unwind_Floorplans, unwind_Units, unwind_LeaseTerms, match, group, projection, sort };

            try
            {


                MongoClient client = new MongoClient(MongoConnectionString);
                MongoServer server = client.GetServer();
                MongoDatabase db = server.GetDatabase(Database);
                MongoCollection collection = db.GetCollection(ColAvailability);
                AggregateArgs args = new AggregateArgs();
                args.Pipeline = pipeline;
                IEnumerable<BsonDocument> aggregates = collection.Aggregate(args);

                if ((aggregates != null) && (aggregates.Count<BsonDocument>() != 0))
                {
                    foreach (var fp in aggregates)
                    {

                        FloorplanAggregate floor = Newtonsoft.Json.JsonConvert.DeserializeObject<FloorplanAggregate>(fp.ToString());
                        //LevelOneAPProperty.Floorplan floor = Newtonsoft.Json.JsonConvert.DeserializeObject<LevelOneAPProperty.Floorplan>(fp.ToString());
                        //floorplansFound.Add(floor);


                        LevelOneAPProperty.Floorplan fpB = new LevelOneAPProperty.Floorplan();
                        fpB.Bed = floor.Bed;
                        fpB.Bath = floor.Bath;
                        fpB.AvailableNow = floor.AvailableNow;
                        fpB.Available30 = floor.Available30;
                        fpB.Available60 = floor.Available60;
                        fpB.SquareFeetMin = floor.SquareFeetMin;
                        fpB.SquareFeetMax = floor.SquareFeetMax;
                        fpB.EffectiveRentMax = floor.MarketRentMax;
                        fpB.EffectiveRentMin = floor.MarketRentMin;
                        fpB.MarketRentMax = floor.MarketRentMax;
                        fpB.MarketRentMin = floor.MarketRentMin;

                        FloorPlanByBedBath.Add(fpB);

                    }
                }//aggregates null check
            }
            catch (Exception e)
            {
                // not sure how to handle this just yet.
            }
            //probably need to move this to floorplansbedandbath aggregate

            property.Floorplans = FloorPlanByBedBath;
            property.PropertyID = propertyId;

            return property;



        }

        #endregion
    }

}