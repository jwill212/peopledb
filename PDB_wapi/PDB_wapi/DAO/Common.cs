﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi
{
    public static  class Common
    {
        internal static string GetTitleDate(string startDate)
        {
            string thisDate = startDate.Substring(0, 4) + "-" + startDate.Substring(4, 2) + "-" + startDate.Substring(6, 2);

            DateTime d1 = DateTime.Parse(thisDate);
            string dd = d1.Day.ToString();
            string month = GetMonthName(d1.Month);
            string thisTitle = dd + "-" + month;
            return thisTitle;
        }
        public static string RemoveHyphen(string name)
        {
          string thisName = name.Replace("-", " ");
          return thisName;
        }

        internal static string GetMonthName(int ascMonth)
        {
            string MonthName = "";
            switch (ascMonth)
            {
                case 1:
                    {
                        MonthName = "Jan";
                        break;
                    }
                case 2:
                    {
                        MonthName = "Feb";
                        break;
                    }
                case 3:
                    {
                        MonthName = "Mar";
                        break;
                    }
                case 4:
                    {
                        MonthName = "Apr";
                        break;
                    }
                case 5:
                    {
                        MonthName = "May";
                        break;
                    }
                case 6:
                    {
                        MonthName = "Jun";
                        break;
                    }
                case 7:
                    {
                        MonthName = "Jly";
                        break;
                    }
                case 8:
                    {
                        MonthName = "Aug";
                        break;
                    }
                case 9:
                    {
                        MonthName = "Sep";
                        break;
                    }
                case 10:
                    {
                        MonthName = "Oct";
                        break;
                    }
                case 11:
                    {
                        MonthName = "Nov";
                        break;
                    }
                case 12:
                    {
                        MonthName = "Dec";
                        break;
                    }
            }

            return MonthName;
        }


        internal static string NextFirstDate(string startDate)
        {
            string thisDate = startDate.Substring(0, 4) + "-" + startDate.Substring(4, 2) + "-" + startDate.Substring(6, 2);

            DateTime d1 = DateTime.Parse(thisDate);
            DateTime d2 = d1.AddDays(7);
            string yy = d2.Year.ToString();
            string mm = d2.Month.ToString();
            if (mm.Length == 1)
                mm = "0" + mm;
            string dd = d2.Day.ToString();
            if (dd.Length == 1)
                dd = "0" + dd;
            string firstOfWeek = yy + mm + dd;
            return firstOfWeek;
        }

        internal static string GetWeekEnd(string startDate)
        {

            string thisDate = startDate.Substring(0, 4) + "-" + startDate.Substring(4, 2) + "-" + startDate.Substring(6, 2);
            DateTime d1 = DateTime.Parse(thisDate);
            DateTime d2 = d1.AddDays(6);
            string yy = d2.Year.ToString();
            string mm = d2.Month.ToString();
            if (mm.Length == 1)
                mm = "0" + mm;
            string dd = d2.Day.ToString();
            if (dd.Length == 1)
                dd = "0" + dd;
            string endOfWeek = yy + mm + dd;
            return endOfWeek;
        }
        internal static string GetFristDate(string startDate)
        {
            string FirstDate = "";

            int yyyy = Int32.Parse(startDate.Substring(0, 4));
            int mm = Int32.Parse(startDate.Substring(4, 2));
            int dd = Int32.Parse(startDate.Substring(6, 2));

            DateTime d = new DateTime(yyyy, mm, dd);
            string thisDay = d.DayOfWeek.ToString();
            while (thisDay != "Sunday")
            {
                d = d.AddDays(-1);
                thisDay = d.DayOfWeek.ToString();
            }
            string MM = d.Month.ToString();
            if (MM.Length == 1)
                MM = "0" + MM;

            string DD = d.Day.ToString();
            if (DD.Length == 1)
                DD = "0" + DD;

            FirstDate = d.Year.ToString() + MM + DD;

            return FirstDate;
        }
        internal static string GetLastDate(string endDate)
        {
            string LastDate = "";

            int yyyy = Int32.Parse(endDate.Substring(0, 4));
            int mm = Int32.Parse(endDate.Substring(4, 2));
            int dd = Int32.Parse(endDate.Substring(6, 2));

            DateTime d = new DateTime(yyyy, mm, dd);
            string thisDay = d.DayOfWeek.ToString();
            while (thisDay != "Saturday")
            {
                d = d.AddDays(1);
                thisDay = d.DayOfWeek.ToString();
            }
            string MM = d.Month.ToString();
            if (MM.Length == 1)
                MM = "0" + MM;

            string DD = d.Day.ToString();
            if (DD.Length == 1)
                DD = "0" + DD;

            LastDate = d.Year.ToString() + MM + DD;

            return LastDate;
        }
    }
}