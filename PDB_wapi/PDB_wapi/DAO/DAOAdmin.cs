﻿using PetaPoco;

namespace PDB_wapi
{
    public class DAOAdmin
    {
        Database _db = null;

        public DAOAdmin()
        {
            _db = new Database("primaryDBConn");
        }

        public int GetUserReportPermission(int PammUserId, string ReportURL)
        {
            try
            {
                return _db.ExecuteScalar<int>(@"SELECT permissionId from Reports.dbo.ReportAllowedSubscribers 
JOIN reports.dbo.ReportSection on reportSectionID = sectionID
where userID = @0 and xslTemplate=@1 and sectionIsEnabled = 1", PammUserId, ReportURL);
            }
            catch { return 0; }
        }
    }
}