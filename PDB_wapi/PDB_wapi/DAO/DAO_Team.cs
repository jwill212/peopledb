﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PDB_wapi.DAO
{
    public class BusinessLineManager
    {
        public BusinessLineManager() { Managers = new List<NameIDPair>(); }

        public int ID { get; set; }
        public string Name { get; set; }
        public List<NameIDPair> Managers { get; set; }

    }

    public class NameIDPair
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class TeamDAO
    {
        static string connString = ConfigurationManager.AppSettings["primaryDBConn"].ToString();
        static Database db = new Database("primaryDBConn");

        public static BusinessLineManager[] GetBusinessLineMgrForLocation(string Location)
        {
            using(SqlConnection Connection = new SqlConnection(connString))
            {
                string sql = @"select distinct bl.BusinessLineId, bl.BusinessLine, 0 as BLKey, NULL as Manager, NULL as LocationID from KPI_BusinessLine bl
join KPI_BusinessLineMgr m on bl.BusinessLine = m.BusinessLine where m.LocationId = @loc
UNION ALL
SELECT bl.BusinessLineId, null, BLKey, m.Manager, m.LocationId from KPI_BusinessLine bl
join KPI_BusinessLineMgr m on bl.BusinessLine = m.BusinessLine
where m.LocationId = @loc
order by BusinessLineId, BLKey";

                List<BusinessLineManager> ret = new List<BusinessLineManager>();
                SqlCommand command = new SqlCommand(sql, Connection);
                command.Parameters.Add(new SqlParameter("loc", Location));
                command.CommandType = CommandType.Text;
                command.Connection.Open();
                SqlDataReader myReader = command.ExecuteReader();

                BusinessLineManager currentBusinessLine = null;
                while (myReader.Read())
                {
                    if (DBNull.Value != myReader["BusinessLine"]) {
                        currentBusinessLine = new BusinessLineManager();
                        currentBusinessLine.ID = int.Parse(myReader["BusinessLineId"].ToString());
                        currentBusinessLine.Name = myReader["BusinessLine"].ToString();
                        ret.Add(currentBusinessLine);
                    }
                    else if(currentBusinessLine != null)
                    {
                        currentBusinessLine.Managers.Add(new NameIDPair()
                        {
                            ID = int.Parse(myReader["BLKey"].ToString()),
                            Name = myReader["Manager"].ToString()
                        });
                    }
                
                }

                return ret.ToArray();
            }
        }

        internal void UpdateUserTeam(int pDBId, string supervisor1)
        {
            db.Execute("DELETE FROM PDB_Manager_Employee WHERE EmployeeID = @0", pDBId);
            db.Execute(@"INSERT INTO [dbo].[PDB_Manager_Employee]
           ([ManagerID]
           ,[EmployeeID])
     VALUES
           (@0,@1)", supervisor1, pDBId);
        }

        public static NameIDPair[] GetTeamLeads(string location)
        {
            var activeFilter = DAODates.Replace("active", "", null);
            string sql = String.Format(@"SELECT e.PDBId [ID], e.[Employee Name] [Name]
from pdb_employee e
where 
	(position like 'TL-%' or position = 'TL' or position like 'opm%' or position like 'TAC%')
	and Location = @0
    and {0}
order by e.[Employee Name]",activeFilter);
            return db.Fetch<NameIDPair>(sql, location).ToArray();
        }

        public static void UpdateTeamLead(PDB_Employee agent, int newTeamLeadID)
        {
            db.Execute(@"DELETE FROM PDB_Manager_Employee where EmployeeID = @0", agent.PDBId);
            db.Execute(@"INSERT INTO PDB_Manager_Employee ([ManagerID],[EmployeeID]) VALUES (@0,@1)", newTeamLeadID, agent.PDBId);
        }
    }
}