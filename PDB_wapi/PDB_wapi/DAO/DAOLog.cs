﻿using PDB_wapi.Data;
using PetaPoco;
using System;
using System.IO;
using System.Security.Principal;
using System.Xml;
using System.Xml.Serialization;

namespace PDB_wapi
{
    public class DAOLog<T>
    {
        Database _db = null;

        private ReportSubscriberPrincipal _user { get { return (ReportSubscriberPrincipal)System.Threading.Thread.CurrentPrincipal; } }

        public DAOLog()
        {
            _db = new Database("primaryDBConn");
        }

        public void LogUpdate(T oldObj, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            DAODates _d = new DAODates();
            try
            {
                _db.OpenSharedConnection();
                _db.BeginTransaction();
                _db.Insert(new PDB_Log()
                {
                    Change = "Update",
                    Type = oldObj.GetType().ToString(),
                    CallingMethod = memberName,
                    TimeStamp = DateTime.Now,
                    UserID = _user.UserID,
                    UserName = _user.UserName,
                    OldObj = XMLSerialize(oldObj)
                });
            }
            finally
            {
                _db.CompleteTransaction();
                _db.CloseSharedConnection();
            }
        }

        private string XMLSerialize(T obj)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            var xml = "";

            using (var sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                xsSubmit.Serialize(writer, obj);
                xml = sww.ToString(); // Your XML
            }
            return xml;
        }

        public void LogInsert(string newId, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            _db.Insert(new PDB_Log()
            {
                Change = "Insert",
                Type = typeof(T).ToString(),
                CallingMethod = memberName,
                TimeStamp = DateTime.Now,
                UserID = _user.UserID,
                UserName = _user.UserName,
                OldObj = String.Format("<ID>{0}</ID>", newId)
            });
        }

        public void LogDelete(string id, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            _db.Insert(new PDB_Log()
            {
                Change = "Delete",
                Type = typeof(T).ToString(),
                CallingMethod = memberName,
                TimeStamp = DateTime.Now,
                UserID = _user.UserID,
                UserName = _user.UserName,
                OldObj = String.Format("<ID>{0}</ID>", id)
            });
        }
    }
}