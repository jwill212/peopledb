﻿
using System;

namespace PDB_wapi
{
    [PetaPoco.TableName("PDB_Log")]
    [PetaPoco.PrimaryKey("LogID")]
    public class PDB_Log
    {
        public Int64 UserID { get; set; }

        public string UserName { get; set; }

        public string Type { get; set; }

        public string CallingMethod { get; set; }
        
        public string Change { get; set; }

        public string LogID { get; set; }

        public DateTime TimeStamp { get; set; }

        public string OldObj { get; set; }
    }

}
