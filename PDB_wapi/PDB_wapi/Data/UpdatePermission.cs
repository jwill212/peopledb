﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi
{
    public class UpdatePermisssion
    {
        #region Privates
       
       
        private int _reportSectionID;
        private int _userID;
        private int _permissionId;
    


        #endregion

        #region Properties

       
        [System.Xml.Serialization.XmlElement("ReportSectionID")]
        public int ReportSectionID
        {
            get { return _reportSectionID; }
            set { _reportSectionID = value; }
        }
        [System.Xml.Serialization.XmlElement("UserID")]
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }   
        [System.Xml.Serialization.XmlElement("PermissionId")]
        public int PermissionId
        {
            get { return _permissionId; }
            set { _permissionId = value; }
        }
         

        #endregion

    }
  
}