﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi
{
    public class ReportPermission
    {

        #region Privates

        private int _PermissionId;
        private string _PermissionDesc;
        private int _MDI = 0;
        #endregion

        #region Properties

        [System.Xml.Serialization.XmlElement("PermissionId")]
        public int PermissionId
        {
            get { return _PermissionId; }
            set { _PermissionId = value; }
        }
        [System.Xml.Serialization.XmlElement("PermissionDesc")]
        public string PermissionDesc
        {
            get { return _PermissionDesc; }
            set { _PermissionDesc = value; }
        }
        [System.Xml.Serialization.XmlElement("MDI")]
        public int MDI
        {
            get { return _MDI; }
            set { _MDI = value; }
        }  
        #endregion
    }
}