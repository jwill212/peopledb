﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi.Data
{
    public class StatisticsResults
    {
        public string grouping { get; set; }
        public string location { get; set; }
        public int count { get; set; }
    }

    public class StatisticsResultObj
    {
        private StatisticsResultObj() { CategoryList = new List<StatCategory>(); }

        public List<StatCategory> CategoryList { get; set; }

        public StatCategory Total { get; set; }

        public static StatisticsResultObj CreateFromQueryResults(List<StatisticsResults> results)
        {
            var retVal = new StatisticsResultObj();
            if (results != null && results.Count > 0) {
                var curObj = new StatCategory(results[0].grouping);
                retVal.CategoryList.Add(curObj);
                for (int index = 0; index < results.Count; ++index)
                {
                    if (!curObj.Name.Equals(results[index].grouping))
                    {
                        curObj = new StatCategory(results[index].grouping);
                        if (curObj.Name != "Total") { retVal.CategoryList.Add(curObj); }
                        else { retVal.Total = curObj; }
                    }
                    var val = results[index].count == 0 ? "" : results[index].count.ToString();
                    curObj.Values.Add(new KeyValuePair<string, string>(results[index].location, val));
                }
            }
            return retVal;
        }
    }

    public class StatCategory
    {
        public StatCategory(string name)
        {
            Values = new List<KeyValuePair<string, string>>();
            Name = name;
        }
        public string Name { get; private set; }
        public List<KeyValuePair<string,string>> Values { get; set; }
    }
}