﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi.Data
{
    public class PammUser
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}