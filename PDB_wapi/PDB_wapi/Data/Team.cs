﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi.Data
{
    public class Team
    {
        public bool Home { get; set; }
        public string BusinessLine { get; set; }
        public string Manager { get; set; }
        public int ManagerID { get; set; }
        public string TeamLead { get; set; }
        public int TeamLeadID { get; set; }
    }
}