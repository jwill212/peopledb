﻿
namespace PDB_wapi.Data
{
    public class SkillGroupSkill
    {
        public int ID { get; set; }
        public string BusinessLine { get; set; }
        public string GroupName { get; set; }
        public int SkillID { get; set; }
        public string  SkillName { get; set; }
        public int Priority { get; set; }
        public string Action { get; set; }
    }

    public class SkillGroupTableRow
    {
        public int ID { get; set; }
        public string BusinessLine { get; set; }
        public string GroupName { get; set; }
        public int SkillCount { get; set; }
    }

    public class AgentSkillGroups
    {
        public int SkillGroupId { get; set; }
        public string PhoneLogin { get; set; }
        public string GroupName { get; set; }
    }
}