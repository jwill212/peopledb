﻿using System;

namespace PDB_wapi
{
    [PetaPoco.TableName("PDB_Codes")]
    [PetaPoco.PrimaryKey("PDBCodeId")]
    public class PDB_Codes
    {
        [PetaPoco.Column("PDBCodeId")]
        public int ID { get; set; }

        [PetaPoco.Column("PDBCodeTitle")]
        public string Title { get; set; }

        [PetaPoco.Column("PDBCodeType")]
        public string Type { get; set; }

        [PetaPoco.Column("PDBCodeAbrv")]
        public string Abbrev { get; set; }
    }

}
