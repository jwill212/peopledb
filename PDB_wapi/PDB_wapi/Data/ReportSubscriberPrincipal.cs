﻿using System;
using System.Security.Principal;

namespace PDB_wapi.Data
{
    public class ReportSubscriberPrincipal : IPrincipal
    {
        private GenericPrincipal _basePrincipal;
        private ReportAllowedSubscribers _reportPermissions;

        public string UserName
        {
            get { return _basePrincipal.Identity.Name; }
        }

        public int UserID { get; private set; }

        public ReportSubscriberPrincipal(GenericPrincipal basePrincipal, int userID)
        {
            _basePrincipal = basePrincipal;
            UserID = userID;
        }

        public IIdentity Identity
        {
            get { return _basePrincipal.Identity; }
        }

        public bool IsInRole(string role)
        {
            return _basePrincipal.IsInRole(role);
        }
    }
}