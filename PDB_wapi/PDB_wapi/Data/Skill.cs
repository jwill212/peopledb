﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDB_wapi.Data
{
    public class Skill
    {
        public int value { get; set; }
        public string label { get; set; }
    }
}