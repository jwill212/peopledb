﻿using System;
using System.Security.Principal;

namespace PDB_wapi
{
    public class ReportAllowedSubscribers
    {
        #region Privates
        //FROM REPORTSECTION
        private string _sectionTitle;

        //FROM REPORTALLOWEDSCUBSCRIBERS
        private int _reportSectionID;
        private int _userID;
        private int _reportRoleID;
        private int _scheduleID;
        private bool _isEmailOnly;
        private int _permissionId;
        
        //FROM PAAMM.userlogin
        private int _biLoginID;
        private string _vcLoginName;
        private string _vcFirstName;
        private string _vcLastName;
     

        #endregion

        #region Properties

        public int ManagementCompanyID;

        [System.Xml.Serialization.XmlElement("SectionTitle")]
        public string SectionTitle
        {
            get { return _sectionTitle; }
            set { _sectionTitle = value; }
        }
        [System.Xml.Serialization.XmlElement("ReportSectionID")]
        public int ReportSectionID
        {
            get { return _reportSectionID; }
            set { _reportSectionID = value; }
        }
        [System.Xml.Serialization.XmlElement("UserID")]
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        [System.Xml.Serialization.XmlElement("ReportRoleID")]
        public int ReportRoleID
        {
            get { return _reportRoleID; }
            set { _reportRoleID = value; }
        }

        [System.Xml.Serialization.XmlElement("ScheduleID")]
        public int ScheduleID
        {
            get { return _scheduleID; }
            set { _scheduleID = value; }
        }
        [System.Xml.Serialization.XmlElement("IsEmailOnly")]
        public bool IsEmailOnly
        {
            get { return _isEmailOnly; }
            set { _isEmailOnly = value; }
        }
        [System.Xml.Serialization.XmlElement("PermissionId")]
        public int PermissionId
        {
            get { return _permissionId; }
            set { _permissionId = value; }
        }

        [System.Xml.Serialization.XmlElement("biLoginID")]
        public int biLoginID
        {
            get { return _biLoginID; }
            set { _biLoginID = value; }
        }

        [System.Xml.Serialization.XmlElement("vcLoginName")]
        public string vcLoginName
        {
            get { return _vcLoginName; }
            set { _vcLoginName = value; }
        }

        [System.Xml.Serialization.XmlElement("vcFirstName")]
        public string vcFirstName
        {
            get { return _vcFirstName; }
            set { _vcFirstName = value; }
        }

        [System.Xml.Serialization.XmlElement("vcLastName")]
        public string vcLastName
        {
            get { return _vcLastName; }
            set { _vcLastName = value; }
        }
        #endregion

    }
    public class ReportAllowedSubscribersCollection : System.Collections.Generic.List<ReportAllowedSubscribers>
    {
    }
}