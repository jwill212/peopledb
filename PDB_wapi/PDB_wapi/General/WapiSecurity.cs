﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Http.Headers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PDB_wapi.Controllers
{
    public static class WapiSecurity
    {
        public static int UID;
        public static string LastUser;
        public static int IsAuthorized(HttpRequestMessage request)
        {
            
            string FunctionType = request.Method.ToString();

           
            //GET THE USER'S MANAGEMENT COMPANY ID
            int MID = CookieHandler.BuildUserFromCookie(request);

            return MID;
        }
       
       
    }
}