﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace PDB_wapi.Controllers
{
    public static class CookieHandler
    { 
        internal static int BuildUserFromCookie(HttpRequestMessage request)
        {
           string CurrentDomain = request.RequestUri.Host;

            int MID = 0;
           
            try
            {
                CookieHeaderValue portalCookie = request.Headers.GetCookies().FirstOrDefault();
                if (portalCookie != null)
                {
                    string VALUE = portalCookie["MID"].Value;
                    MID = Int32.Parse(VALUE);

                }
            }
            catch
            {
                
            }
            return MID;
        }
        internal static int GetPammIdFromCookie(HttpRequestMessage request)
        {
            string CurrentDomain = request.RequestUri.Host;

            int UID = 0;

            try
            {

                CookieHeaderValue portalCookie = request.Headers.GetCookies("UID").FirstOrDefault();
                if (portalCookie != null)
                {
                    string VALUE = portalCookie["UID"].Value;
                    UID = Int32.Parse(VALUE);

                }

            }
            catch
            {

            }
            return UID;
        }

    }
}