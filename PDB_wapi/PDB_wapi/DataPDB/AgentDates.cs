﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace PDB_wapi
{
    [PetaPoco.TableName("PDB_Employee")]
    [PetaPoco.PrimaryKey("PDBId")]
    public class PDB_Employee
    {
        public PDB_Employee()
        {
            Active = true;
            PammId = "0";
        }

        #region Properties

        [System.Xml.Serialization.XmlElement("EmpId")]
        public string EmpID { get; set; }

        [System.Xml.Serialization.XmlElement("PDBId")]
        public int PDBId { get; set; }

        [System.Xml.Serialization.XmlElement("EmployeeName")]
        [PetaPoco.Column(Name = "Employee Name")]
        public string EmployeeName { get; set; }

        [System.Xml.Serialization.XmlElement("StartDate")]
        [PetaPoco.Column(Name = "Start Date")]
        public string StartDate {
            get { return _startDate == "1/1/1900" || _startDate == "" ? null : _startDate; }
            set { _startDate = GetDateString(value); }
        }
        private string _startDate;

        [System.Xml.Serialization.XmlElement("Position")]
        public string Position { get; set; }

        [System.Xml.Serialization.XmlElement("SoloDate")]
        [PetaPoco.Column(Name = "Solo Date")]
        public string SoloDate {
            get { return _soloDate == "1/1/1900" || _soloDate == "" ? null : _soloDate; }
            set { _soloDate = GetDateString(value); }
        }
        private string _soloDate;

        [System.Xml.Serialization.XmlElement("Status")]
        public string Status { get; set; }

        [System.Xml.Serialization.XmlElement("Resignation")]
        public string Resignation {
            get { return _resDate == "1/1/1900" || _resDate == "" ? null : _resDate; }
            set { _resDate = GetDateString(value); }
        }
        private string _resDate;

        [System.Xml.Serialization.XmlElement("CostCenter")]
        [PetaPoco.Column(Name = "Cost Center")]
        public string CostCenter { get; set; }

        [System.Xml.Serialization.XmlElement("Training")]
        public string Training { get; set; }

        [System.Xml.Serialization.XmlElement("Supervisor")]
        [PetaPoco.ResultColumn()]
        public string Supervisor { get; set; }

        [System.Xml.Serialization.XmlElement("Production")]
        public string Production { get; set; }

        [System.Xml.Serialization.XmlElement("CPLevel")]
        [PetaPoco.Column(Name = "CP Level")]
        public string CPLevel { get { return Position; } set {;} }

        [System.Xml.Serialization.XmlElement("StatusInfo")]
        [PetaPoco.Column(Name = "Status Info")]
        public string StatusInfo { get; set; }

        [System.Xml.Serialization.XmlElement("Location")]
        public string Location { get; set; }

        [System.Xml.Serialization.XmlElement("Supervisor1")]
        public string Supervisor1 { get; set; }

        [System.Xml.Serialization.XmlElement("Skill")]
        [PetaPoco.Column(Name = "SKL")]
        public string Skill { get; set; }

        [System.Xml.Serialization.XmlElement("Team")]
        [PetaPoco.Column(Name = "TEAM")]
        public string Team { get; set; }

        [System.Xml.Serialization.XmlElement("SkillGroup")]
        [PetaPoco.ResultColumn]
        public string SkillGroup { get; set; }

        [System.Xml.Serialization.XmlElement("PammId")]
        public string PammId { get; set; }

        [System.Xml.Serialization.XmlElement("Active")]
        public bool Active { get; set; }
        
        [PetaPoco.ResultColumn]
        public string PhoneLogin { get; set; }
        #endregion

        internal string GetDateString(string val)
        {
            DateTime outVal;
            if(DateTime.TryParse(val, out outVal))
            {
                return outVal.ToShortDateString();
            }
            return "";
        }
        
        internal string[] GetComparison(PDB_Employee updatedObj)
        {
            List<string> _differences = new List<string>();
            Type sourceType = this.GetType();
            Type destinationType = updatedObj.GetType();

            if (sourceType == destinationType)
            {
                PropertyInfo[] sourceProperties = sourceType.GetProperties();
                foreach (PropertyInfo pi in sourceProperties)
                {
                    var val1 = sourceType.GetProperty(pi.Name).GetValue(this, null);
                    var val2 = destinationType.GetProperty(pi.Name).GetValue(updatedObj, null);
                    if (val1 != null && val2 != null && !val1.Equals(val2))
                    {
                        _differences.Add(String.Format("{0}: Changed [{1}] to [{2}]", pi.Name, val1.ToString(), val2.ToString()));
                    }
                }
            }
            else
            {
                throw new ArgumentException("Comparison object must be of the same type.", "comparisonObject");
            }

            return _differences.ToArray();
        }
    }
}