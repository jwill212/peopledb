﻿using PDB_wapi.Controllers;
using PDB_wapi.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace PDB_wapi
{
    public enum PDB_AccessLevels { All=1, Edit=2, Admin=3 }
    public class PDB_AuthorizedAttribute : AuthorizeAttribute
    {
        public PDB_AccessLevels Access { get; set; }

        public PDB_AuthorizedAttribute(PDB_AccessLevels access = PDB_AccessLevels.All)
        {
            Access = access;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            ApiController controller = (ApiController)actionContext.ControllerContext.Controller;
#if DEBUG
            //System.Threading.Thread.CurrentPrincipal = new ReportSubscriberPrincipal((GenericPrincipal)controller.User, 1);
            //return true;
#endif
            DAOAdmin admin = new DAOAdmin();
            var pammID = CookieHandler.GetPammIdFromCookie(actionContext.Request);
            if (controller.User.Identity.IsAuthenticated) {
                var permissionLevel = admin.GetUserReportPermission(pammID, actionContext.Request.Headers.Referrer.LocalPath);
                if (permissionLevel < (int)Access) { return false; }
                System.Threading.Thread.CurrentPrincipal = new ReportSubscriberPrincipal((GenericPrincipal)controller.User, pammID);
                return true;
            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            ApiController controller = (ApiController)actionContext.ControllerContext.Controller;
            if (controller.User.Identity.IsAuthenticated)
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden,"User does not have permission");
            else
                base.HandleUnauthorizedRequest(actionContext);

        }
    }
}