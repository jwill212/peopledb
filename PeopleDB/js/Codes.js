function Codes() {
    var categories = [];
    var me = this;
    this.GetCodes = function () {
        if (categories.length == 0) {
            categories = GetData("Codes");
        }
        ShowPageTitle("Codes Admin");
        $("#page-PeopleList").show().load("tab_Setup_Codes.html?ver=2.0.0.2", LoadResults);
    };
    this.GetCodeDetails = function () {
        var selectedGroup = $("#selCodeGroups").val();
        var data = GetData("Codes", "id", selectedGroup);
        var template = $("#codeTbl_tmpl").html();
        $("#codesTbl tbody").html(Mustache.to_html(template, data));
        $("#codesInptTitle").val("").focus();
        $("#codesInptAbbrev").val("");
    }
    this.New = function () {
        alert('New Code');
    }
    this.Add = function (e) {
        e.preventDefault();
        var title = $("#codesInptTitle").val();
        var abbrev = $("#codesInptAbbrev").val();
        var selectedGroup = $("#selCodeGroups").val();
        var url = "Codes?group=" + selectedGroup + "&title=" + title + "&abbrev=" + abbrev;
        if (PostData(url)) {
            me.GetCodeDetails();
        }
        return false;
    }
    this.Delete = function (id) {
        if (DeleteData("Codes","id",id)) {
            me.GetCodeDetails();
        }
        return false;
    }

    function LoadResults() {
        var template = $("#codeDDL").html();
        $("#selCodeGroups").html(Mustache.to_html(template, categories));
        me.GetCodeDetails();
        $("#codesFrm").submit(me.Add);
    }
}

var codeObj = new Codes();
