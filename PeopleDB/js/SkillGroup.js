SkillGroup = {
  GroupID: 0,
  skillList: null,
  skillChanges: [],
  GetGroups: function () {
    var data = GetData("Skills/SkillGroupSummary");
    if (data) {
        ShowPageTitle("SkillGroups", function (e) {
            e.preventDefault();
            $("#dialogSkillGroup").data('groupID',0).dialog('open');
        });

        var template = $("#tmpl_SkillsTable").html();
        $("#page-PeopleList").html(Mustache.to_html(template, data)).show();
    }
  },
  LoadGroup: function (groupID) {
    $("#dialogSkillGroup")
        .data('groupID', groupID)
        .dialog('open');
  },
  AddSkill: function (e) {
    e.preventDefault()
    var num = $("#skillNum");
    var name = $("#sklName");
    var priority = $("#sklPriority");
    var data = {
        ID: SkillGroup.GroupID,
        BusinessLine: $("#input_BL").val(),
        GroupName: $("#grpName").val(),
        SkillID: num.html(),
        SkillName: name.val(),
        Priority: priority.val(),
        Action: "Add"
    };

    var template = $("#tmpSkillRow").html();
    var row = $(Mustache.to_html(template, data));
    $("#tblSkills tbody").append(row);
    var delBtn = row.find(".skillDelBtn");
    delBtn[0].data = data;
    delBtn.button({
        icon: "ui-icon-gear"
    }).on("click", SkillGroup.RemoveSkillChange);
    SkillGroup.skillChanges.push(data);

    num.html("");
    name.val("").focus();
    priority.val("");

    return false;
  },
  RemoveSkillChange: function(){
      var index = SkillGroup.skillChanges.indexOf(this.data);
      if (index > -1) {
          SkillGroup.skillChanges.splice(index, 1);
      }
      $(this.parentNode.parentNode).remove();
  },
  SetupBusinessLines: function() {
    var dd = $('#input_BL');
    if (dd.html() === "") {
        LoadCodeToDropDown(dd, "BL", "tmpl_codeDD_title");
        dd.on("change", function () {
            $("#skillGrpForm").find("[disabled]").prop("disabled", false);
            var params = "businessLine=" + $(this).val();
            SkillGroup.skillList = GetData("Skills/GetSkills", params, 0);
            $("#sklName").autocomplete({
                source: SkillGroup.skillList,
                select: SkillGroup.SelectAutocomplete,
                focus: function (e, u) { e.preventDefault(); if (u.item) { $(this).val(u.item.label); } }
            });
        });
    }
  },
  SelectAutocomplete: function (event, ui) {
    event.preventDefault();
    $(this).val(ui.item.label);
    $("#skillNum").html(ui.item.value);
    return false;
  },
  LoadDialog: function () {
      $("#skillGrpForm").submit(SkillGroup.AddSkill);
      SkillGroup.SetupBusinessLines();
      SkillGroup.GroupID = $(this).data('groupID') || 0;
      if (SkillGroup.GroupID > 0) {
          var template = $("#tmpSkillRow").html();
          var data = GetData('Skills/SkillGroupList', "groupID=" + SkillGroup.GroupID, 0);
          $("#input_BL").val(data[0].BusinessLine).trigger("change").prop("disabled", true);
          $("#grpName").val(data[0].GroupName).prop("disabled", true);
          $("#btnDelSkillGrp").show().on("click", SkillGroup.DeleteGroup);
          for (var i = 0; i < data.length; i++) {
              $("#tblSkills tbody").append(Mustache.to_html(template, data[i]));
          }
          $(".skillDelBtn").button({
              icon: "ui-icon-gear"
          }).on("click", SkillGroup.DeleteSkill);
      }
      $("#btnSaveSkillGrp").on("click", SkillGroup.SaveGroup);
  },
  SaveGroup: function () {
      if (SkillGroup.GroupID === 0 && SkillGroup.skillChanges.length === 0) {

      }
      if (SkillGroup.skillChanges.length > 0 && PostData("Skills/SaveSkill", JSON.stringify(SkillGroup.skillChanges))) {
          SkillGroup.skillChanges = [];
          $("#dialogSkillGroup").dialog("close");
      }
  },
  DeleteGroup: function () {
      if (confirm("Are you sure you want to delete this group?  This will affect the agents assigned to this group.")) {
          if (DeleteData("Skills/Delete", "groupID=" + SkillGroup.GroupID)) {
              $("#dialogSkillGroup").dialog("close");
          }
      }
  },
  DeleteSkill: function () {
      var data = {
          ID: SkillGroup.GroupID,
          BusinessLine: $("#input_BL").val(),
          GroupName: $("#grpName").val(),
          SkillID: $(this).attr("skillID"),
          SkillName: "",
          Priority: 0,
          Action: "Delete"
      };
      SkillGroup.skillChanges.push(data);
      $(this.parentNode.parentNode).remove();
  },
  ShowAgentList: function (groupID) {
      $("#dialogSG_AgentList")
          .data('groupID', groupID)
          .dialog('open');
  },
  LoadAgentList: function () {
      SkillGroup.GroupID = $(this).data('groupID') || 0;
      var data = GetData('Skills/AgentList', "id", SkillGroup.GroupID);
      var template = $("#tmplAgentListRow").html();
      $("#tbl_dSGAL tbody").append(Mustache.to_html(template, data));
  },
  CopyAgents: function (btn) {
      var row = $(btn.parentNode.parentNode);
      var textCell = row.find(".agentLogin");
      selectText(textCell[0]);
      try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Copying text command was ' + msg);
          if (successful) {
              showInfo("Text copied");
              row.addClass("ok");
          }
      } catch (err) {
          console.log('Oops, unable to copy');
          showError("Unable to copy!  Press Ctrl-C to manually copy.")
      }
  }
}

function selectText(node) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNodeContents(node);
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
    }
}

$(function () {
    $("#dialogSkillGroup").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "explode",
            duration: 300
        },
        minWidth: 500,
        open: function (e, u) {
            $(this).load("dialog_SkillGroup.html", SkillGroup.LoadDialog);
        },
        beforeClose: function(e,u){
            if (SkillGroup.skillChanges.length > 0 && confirm("Are you sure you want to close without saving?")) {
                SkillGroup.skillChanges = [];
                return true;
            }
            else if (SkillGroup.skillChanges.length == 0) {
                return true;
            }
            return false;
        },
        close: function (e, u) {
            $(this).html('');
            SkillGroup.GetGroups();
        }
    });

    $("#dialogSG_AgentList").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "explode",
            duration: 300
        },
        buttons: { "OK": function () { $(this).dialog("close");}},
        minWidth: 1000,
        open: function (e, u) {
            $(this).load("dialog_SkillGroup_AgentList.html?ver=2.0.0.2", SkillGroup.LoadAgentList);
        },
        beforeClose: function(e,u){
        },
        close: function (e, u) {
            $(this).html('');
        }
    });
});
