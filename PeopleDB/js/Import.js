function Import() {
    var me = this;
    
    this.SetupPammImport = function () {
        ShowPageTitle("Import from PAMMM");
        $("#page-PeopleList").show().load("tab_ImportFromPamm.html?ver=2.0.0.2", LoadResults);
    }
    this.ImportPammUser = function(pammID){
        if(PostData("Admin/ImportPammUser/" + pammID, null)){
            alert("Pamm user imported.");
        }
    }

    function LoadResults() {
        $("#importFrm").submit(SearchPamm);
    }
    function SearchPamm(e) {
        e.preventDefault();
        var search = $("#iptPammSearch").val();
        var data = GetData("Admin/SearchPAMM", "search=" + search, 0);
        if (data.length > 0) {
            var template = $("#resultsRow").html();
            $("#pammResults tbody").html(Mustache.to_html(template, data));
        } else {
            alert("PAMM user not found");
        }
        return false;
    }
}

var importObj = new Import();
