

function showError(msg) {
    $.jGrowl(msg, {
        header: "Exception!",
        sticky: false,
        life: 7000,
        speed: 500,
        theme: 'with-icon',
        position: 'top-right',
        easing: 'easeOutBack',
        animateOpen: {
            height: "show"
        },
        animateClose: {
            opacity: 'hide'
        }
    });
}
function showInfo(msg) {
    $.jGrowl(msg, {
        header: "Information!",           
        sticky: false,
        life: 3000,
        speed: 500,
        theme: 'with-icon',
        position: 'top-left',
        easing: 'easeOutBack',
        animateOpen: {
            height: "show"
        },
        animateClose: {
            opacity: 'hide'
        }
    });
}
function FormatFileDate(fileDate) {
    var yyyy = fileDate.substr(0, 4);
    var mm = fileDate.substr(4, 2);
    var dd = fileDate.substr(6, 2);

    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }

    var thisDate = mm + "-" + dd + "-" + yyyy;

    return (thisDate);
}
function GetFileDate(thisDate) {
    var str = thisDate;
    var n = str.substr(2, 3);
    var d = thisDate;
    var yyyy = d.substr(6, 4);
    var fileDate = yyyy;
    fileDate += d.substr(0, 2) + d.substr(3, 2);

    return fileDate;
}
function MakeDisplayDateFromFile(thisDate) {
    var yyyy = thisDate.substr(0, 4);
    var mm = thisDate.substr(4, 2);
    var dd = thisDate.substr(6, 2);
    var displayDate = mm + "/" + dd + "/" + yyyy;
    return displayDate;
}

function GetFileDateFromDisplay(thisDate) {
   
 
    var arrayOfBits = thisDate.split('/');
   
    var m = arrayOfBits[0]
    var d = arrayOfBits[1];
    var y = arrayOfBits[2];

    if (m.length == 1)
        m = "0" + m;

    if (d.length == 1)
    {
        d = "0" + d;
    }

    fileDate = y + m + d;

    return fileDate;
}
function GetDateTime(){
    var today = new Date();
    var yyyy = today.getFullYear();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();
    var ms = today.getMilliseconds();
       
    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    if (hh < 10) { hh = '0' + hh }    
    if (min < 10) { min = "0" + min}
    if (ss < 10) { ss = '0' + ss }
    
    var thisDate = yyyy + "-" + mm + "-" + dd + " " + hh + ":" +  min + ":" + ss + "." + ms;
   
    return (thisDate);
}

function FutureFunction() {
    alert("This feature is coming soon...");
}

function SelectRow(checkBox) {
    if (checkBox.checked) {
        $(checkBox.parentNode.parentNode).addClass("selected");
    } else {
        $(checkBox.parentNode.parentNode).removeClass("selected");
        $("#agentMasterData thead input[type='checkbox']").prop("checked", false);
    }
}

function SelectAllRows(checkBox) {
    if ($(checkBox).prop("checked")) {
        $("tr[PDBId]").addClass("selected").find("input[type='checkbox']").prop("checked", true);
    } else {
        $("tr[PDBId]").removeClass("selected").find("input[type='checkbox']").prop("checked", false);
    }
}
