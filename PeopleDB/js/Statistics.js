function Statistics() {
    var data = {};
    var me = this;
    this.GetHeadcount = function () {
        me.data = GetData("Stats");
        me.data.Locations = [];
        for (var i = 0; i < me.data.CategoryList[0].Values.length; i++) {
            me.data.Locations.push(this.data.CategoryList[0].Values[i].Key);
        }
        ShowPageTitle("Current Head Count");
        $("#page-PeopleList").load("tab_Stats_Current.html?ver=2.0.0.2", me.LoadResults);
    };
    this.LoadResults = function () {
        var template = $("#statRpt_Cur").html();
        $("#statRpt").html(Mustache.to_html(template, me.data));
        $('#page-PeopleList').show();
        $("#statRpt").tablesorter({
            widgets: ['zebra', 'columns']
        });
    }
    this.ShowDetails = function (y, x) {
        var search = "BusinessLine:" + y + " AND Location:" + x + " AND AgentOnly:";
        $("#searchData").val(search);
        peopleObj.GetPeopleList();
        return false;
    }
}

var Stats = new Statistics();
