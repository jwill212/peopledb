$(function () {
    $('#HeaderDiv').hide();
    GetUser();
    $('#HeaderDiv').show();
});

function GetUser() {
    try {
        var data = GetData("admin/CurrentUser", 0);
        $("#lblUser").html(data);
    } catch (err) {
        showError(err);
    }
}

function search() {
    var searchData = $('#searchData').val();
    if (searchData == "")
        return;
    peopleObj.GetPeopleList();
}
