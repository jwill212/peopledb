function People() {
    var CurrentPersonType = "";
    var me = this;
    var CurrentPDBId = 0;
    var BusinessLines = {};
    var agentObj = {
        "PDBId": 0,
        "EmpID": "",
        "EmployeeName": "",
        "StartDate": "",
        "StartDateDisplay": "",
        "Position": "",
        "SoloDate": "",
        "SoloDateDisplay": "",
        "Status": "",
        "Resignation": "",
        "CostCenter": "",
        "Training": "",
        "Supervisor": "",
        "Production": "",
        "CPLevel": "",
        "StatusInfo": "",
        "Location": "",
        "Supervisor1": "",
        "Skill": "",
        "Team": "",
        "SkillGroup": null,
        "PammId": "",
        "Active": true,
        "PhoneLogin":null
    };

    this.BulkEditClick = function () {
        var updateItemCount = $("#agentMasterData tbody").find(":checked").length;
        if (updateItemCount > 0) {
            $("#dialogBulkEdit").dialog("open");
        }
        else {
            alert('Please select one or more employees to update.');
        }
    }
    
    this.GetPeopleList = function (personType) {
        if (personType) { CurrentPersonType = personType; ShowPageTitle(CurrentPersonType, me.NewEmployee); }
        else { ShowPageTitle("Agent Search", me.NewEmployee); }
        $('#FilterDiv').show();
        $('#FilterDivLabel').show();

        var additionalFilter = $('#input_PersonFilter').val();
        var filter = $('#searchData').val();
        if (!filter.includes(":")) { filter = "Agent:" + filter; }
        var parms = "Filter=" + filter;
        if (filter.length > 0 && additionalFilter.length > 0) { parms += " AND "; }
        parms += additionalFilter;

        try {
            var data = GetData("Agents", parms, 0);
            me.DisplayAgentResults(data);
        }
        catch (err) {
            showError("GetPeople: " + err);
        }
    }

    this.PeopleMenuClick = function (filter, personType) {
        $('#searchData').val(filter);
        me.GetPeopleList(personType);
    }

    this.LoadEmployee = function(PDBId){
        CurrentPDBId = PDBId;
        $("#dialogPerson").dialog("open");
    }

    this.LoadDialog = function () {
        ClearAgentForm();
        if (CurrentPDBId == 0) { CurrentPDBId = GetData("Agents/CreateNew"); }
        LoadPositions();
        LoadCostCenters();
        LoadLocations();

        var parms = "";

        if (CurrentPDBId !== 0) {
            try {
                var parms = "PDBID=" + CurrentPDBId;
                var data = GetData("Agents", parms, 0);
                var this_packageRoot = "";
                me.LoadTeam(data[0].Location);
                LoadData(data);
            }
            catch (err) {
                showError("GetPeople: " + err);
            }
        } else {
            LoadData([agentObj]);
        }
        LoadSkills($("#inptSkillGroup"));
        LoadAgentSkillGroup();
    }

    this.DeleteEmployee = function () {
        if (confirm("Are you sure you want to delete this record?")) {
            if (DeleteData("Agents", "PDBId=" + CurrentPDBId)) {
                $("#dialogPerson").dialog("close");
                var test = $("#agentMasterData").find("tr[pdbid='" + CurrentPDBId + "']").remove();
                SetRowStripes("agentMaster");
            }
        }
    }

    this.SaveEmployee = function() {
        var jsonData = BuildAgentDataObj();

        if (PostData("agents", JSON.stringify(jsonData)) == false) {
            showError("Error: Personnel Data was not Saved.");
            return;
        }
        me.GetPeopleList();
        showInfo("Pesonnel Data was Saved!");
        $("#dialogPerson").dialog("close");
    }

    this.NewEmployee = function () {
        me.LoadEmployee(0);
    }

    this.DisplayAgentResults = function(data) {
        if (data.length > 0) {
            var thispackageHtml = Mustache.to_html($("#tmpl_AgentTable").html(), { "agentMaster": data, "RecordCount": data.length });
            $('#page-PeopleList').html(thispackageHtml);
            SetRowStripes("agentMaster");
            $('#page-PeopleList').show();
            AddSort();
        }
        else {
            $('#page-PeopleList').html("<h5>No People found.</h5>");
            $('#page-PeopleList').show();
        }
    }

    this.ShowManagers = function () {
        var businessLine = $("#inptBL option:selected")[0];
        var businessLineTmpl = $("#tmplPRDD").html();
        $("#inptMgr")
            .find("option")
            .remove()
            .end()
            .append("<option></option>")
            .append(Mustache.to_html(businessLineTmpl, businessLine.Managers));
    }

    this.ShowTL = function () {
        var businessLine = $("#inptBL").val();
        var location = $("#input_Location").val();
        var manager = $("#inptMgr").val();
        var ddTmpl = $("#tmplPRDD").html();
        var data = GetData("Team/TeamLead","location=" + location  + "&manager=" + manager, 0)
        $("#inptTeamLead")
            .find("option")
            .remove()
            .end()
            .append("<option></option>")
            .append(Mustache.to_html(ddTmpl, data));
    }
    
    this.AddSkillGroup = function (e) {
        e.preventDefault();
        var data = {
            SkillGroupID: $("#inptSkillGroup").val(),
            GroupName: $("#inptSkillGroup :selected").text(),
            PhoneLogin: $("#inptPhoneLogin").val()
        };
        if (PostData("Skills?AgentID=" + CurrentPDBId + "&SkillGroupID=" + data.SkillGroupID + "&PhoneLogin=" + data.PhoneLogin)) {
            PopulateSkillGrpTbl([data]);
        }
        return false;
    }

    this.DeleteSkillGroup = function (btn, skillGroupID, PhoneLogin) {
        if (DeleteData("Skills", "AgentID=" + CurrentPDBId + "&SkillGroupID=" + skillGroupID + "&PhoneLogin=" + PhoneLogin, null)) {
            $(btn.parentNode.parentNode).remove();
        }
        return false;
    }

    this.Terminate = function () {
        var termDate = $("#input_TermDate").val();
        var reason = $("#input_TermCondition").val();
        var comments = $("#input_TermCommnet").val();
        if (PostData("Agents/Term?Agent=" + CurrentPDBId + "&Date=" + termDate + "&Reason=" + reason + "&Comments=" + comments, null)) {
            showInfo("Termination was Saved!");
            $("#dialogTERM").dialog("close");
            $("#dialogPerson").dialog("close");
        }
    }

    this.LOA = function () {
        var start = $("#input_BeginLOA").val();
        var end = $("#input_EndLOA").val();

        if (PostData("Agents/LOA?Agent=" + CurrentPDBId + "&Start=" + start + "&End=" + end,null)) {
            showInfo("LOA was Saved!");
            $("#dialogLOA").dialog("close");
            $("#dialogPerson").dialog("close");
        }
    }

    this.LoadTeam = function(location) {
        var data = GetData("Team", "location=" + location, 0);
        var template = $("#tmplPRDD").html();
        $("#dpr_TeamLeads").html(Mustache.to_html(template, data));
    }

    this.ChangeBulkEditFocus = function (dd) {
        var selArea = $(dd).val();
        var dd = $("#dbe_dd");
        if (selArea == "tl") {
            var data = GetData("Agents", "Filter=Position:TL AND Active:", 0);
            dd.html(
                Mustache.to_html("{{#.}}<option value='{{PDBId}}'>{{EmployeeName}}</option>{{/.}}", data)
             );
        } else if (selArea == "pos") { LoadCodeToDropDown(dd, "POS", "tmpl_codeDD"); }
        else if (selArea == "cc") { LoadCodeToDropDown(dd, "CC", "tmpl_codeDD"); }
        else if (selArea == "sg") { LoadSkills(dd); }
    }

    ///////////////////////////
    //      Private         //

    function LoadData(data) {
        if (data.length > 0) {
            $.each(data[0], function (key, value) {
                $("[objMap='" + key + "'").val(value);
            });
        }
        else {
            showError("Record Not found!");
        }
    }
    function BuildAgentDataObj() {
        var objToReturn = {};
        $.each(agentObj, function (key, value) {
            var control = $("[objMap='" + key + "'")
            objToReturn[key] = control.length > 0 ? control.val() : agentObj[key];
        });
        return objToReturn;
    }
    function LoadLoa() {
        $("#dialogLOA").dialog("open");
    }
    function CancelLOA() {
        showInfo("LOA was Canceled!")
        $("#dialogLOA").dialog("close");

    }
    function SaveLOA() {
        showInfo("LOA was Saved!")
        $("#dialogLOA").dialog("close");
    }
    
    function LoadPositions() {
        var dd = $('#input_Position');
        if (dd.html() === "") { LoadCodeToDropDown(dd, "POS", "tmpl_codeDD"); }
    }
    function LoadCostCenters() {
        var dd = $('#input_CostCenter');
        if (dd.html() === "") { LoadCodeToDropDown(dd, "CC", "tmpl_codeDD"); }
    }
    function LoadLocations() {
        var dd = $('#input_Location');
        if (dd.html() === "") { LoadCodeToDropDown(dd, "LOC", "tmpl_codeDD_Abbrev"); }
    }
    function LoadSkills(dd) {
        var skillList = GetData("Skills");
        var template = $("#tmplPRSkillsDD").html();
        var result = Mustache.to_html(template, skillList);
        dd.find("option").remove().end().append("<option></option>" + result);
    }
    function LoadAgentSkillGroup() {
        var agentSkills = GetData("Skills/AgentGroups", "id", CurrentPDBId);
        PopulateSkillGrpTbl(agentSkills);
    }
    function PopulateSkillGrpTbl(skillGroups) {
        var template = $("#tmplSG").html();
        var result = Mustache.to_html(template, skillGroups);
        $("#dlgPR_SG tbody").append(result);
    }
}

var peopleObj = new People();


/////////////////////////////////////////////////////////////////////////
////NAVIGATION
/////////////////////////////////////////////////////////////////////////
function GoHome() {
    window.location.replace("/views/WorkforceHome.aspx")
}

function ShowPageTitle(label, addMethod) {
    $('#page-Title').show();
    $('#page-PeopleList').html('');
    $('#FilterDiv').hide();
    $('#FilterDivLabel').hide();
    $('#label_PeopleType').html(label);
    if (addMethod) { $("#newItem_title").show().off("click").on("click", addMethod); }
    else { $("#newItem_title").hide(); }
    if (label == "Agent Search" || label == "Agents") {
        $('#btn_bulkChange').show();
    } else { $('#btn_bulkChange').hide(); }
}

function LoadCodeToDropDown(control, group, templateID) {
    try {
        var template = $("#" + templateID).html();
        var data = GetData("Codes", "id", group);
        control.html(Mustache.render(template, data));
    }
    catch (err) {
        showError("Error getting codes: " + err);
    }
}

//TERMINATION
function LoadTerm() {
    $("#dialogTERM").dialog("open");
}
function CancelTERM() {
    showInfo("Termination was Canceled!");
    $("#dialogTERM").dialog("close");

}
function SaveTERM() {
    peopleObj.Terminate();
}

//LOA
function LoadLoa() {
    $("#dialogLOA").dialog("open");
}
function CancelLOA() {
    showInfo("LOA was Canceled!");
    $("#dialogLOA").dialog("close");
}
function SaveLOA() {
    peopleObj.LOA();
}

/////////////////////////////////////////////////////////
// SETUP 
////////////////////////////////////////////////////////
function ClearAgentForm() {
    $('#label_SkillGroup1').hide();
    $('#label_SkillGroup2').hide();
    $('#label_SkillGroup3').hide();
}
function ViewSkills(id) {
    switch(id)
    {
        case 1:
            {
                $('#label_SkillGroup1').show();
                break;
            }
        case 2:
            {
                $('#label_SkillGroup2').show();
                break;
            }
        case 3:
            {
                $('#label_SkillGroup3').show();
                break;
            }
    }
}

/////////////////////////////////////////////////////////////////////////////
///GENERAL FUNCTIONS
////////////////////////////////////////////////////////////////////////////
    function AddSort() {
        $('table').tablesorter({
            widgets: ['zebra', 'columns']
        });
    }

    function SetRowStripes(id) {
        $('table').trigger('updateAll', [true]);
    }

$(function () {
    var availableTags = ["active:","loa:","terminated:","AgentOnly:","id:","agent:","team:","position:","location:","cost:","skillgroup:","businessline:"];
    $("#searchData").autocomplete({
        source: availableTags
    });

    $('#searchData').keypress(function (e) {
        
        if (e.keyCode == 13) {
            search();
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });
    

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        alert("Internet Explorer is not supported.  Please use Firefox or Chrome.");
        //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    }
});

