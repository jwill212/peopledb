//LOCAL
var wapiService = "/PDBService/api/";

function GetData(ControllerName, Parm, id) {
    //FORMAT Parm: packageMapsId=5&vendorId=2
    // or  where Parm == "id", the method will use the value in the id field as the value passed.
    var Data = "";
    var isGood = false;
 
    urlString = wapiService + ControllerName;
    if (id != undefined) {
        if (Parm == "id") {
            urlString += "/" + id;
        }
        else {
            urlString += "?" + encodeURI(Parm);
        }
    }
   
    $.ajax({
        type: "GET",
        url: urlString,
        data: null,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, textStatus, xhr) {
            isGood = true;
            Data = data;

        },
        error: function (msg) {
            isGood = false;
            //alert(msg.statusText);
            var thisTitle = "GET ERROR WHILE CALLING:  " + urlString;
            BuildError(thisTitle, msg.responseText);

        }
    })
     .fail(function (x, y, z) {
         if (x.status == 401) {
             window.location.replace("/Login.aspx?ReturnUrl=%2fpdb%2fpeopledb.html")
         }
     })
     .complete(function (a, b, c) {
         //alert("complete");
     });

    return Data;
}

function GetExcelData(ControllerName, Parm, id) {
    //FORMAT Parm: packageMapsId=5&vendorId=2
    // or  where Parm == "id", the method will use the value in the id field as the value passed.
    var Data = "";
    var isGood = false;

    urlString = wapiService + ControllerName;
    if (id == undefined) {

    }
    else {

        if (Parm == "id") {
            urlString += "/" + id;
        }
        else {
            urlString += "?" + Parm;
        }
    }

    $.ajax({
        type: "GET",
        url: urlString,
        data: null,
        scriptCharset: "UTF-8",
        dataType: "html",
        async: false,
        success: function (data, textStatus, xhr) {
            isGood = true;
           // var rows = data.split("\n");
            //rows.shift();
            Data = data;

           
           
        },
        error: function (msg) {
            isGood = false;
            //alert(msg.statusText);
            var thisTitle = "GET ERROR WHILE CALLING:  " + urlString;
            BuildError(thisTitle, msg.responseText);

        }
    })
     .fail(function (x, y, z) {
         if (z == "Unauthorized") {
              //alert("fail");
         }
     })
     .complete(function (a, b, c) {
         //alert("complete");
     });

    return Data;
}
//UPDATE
function PutData(ControllerName, dataJson) {
    //FORMAT dataJason:  must be an object 
   
    var isGood = true;
    urlString = wapiService + ControllerName;

   // alert(urlString);
    $.ajax({
        type: "PUT",
        url: urlString,
        data: dataJson,
        async: false,
        success: function (data, textStatus, xhr) {
          //  alert("a");
        },
        error: function (msg) {
           // alert(msg);
            isGood = false;
            var thisTitle = "PUT ERROR WHILE CALLING:  " + urlString;
            BuildError(thisTitle, msg.responseText);

        }
    })
     .fail(function (x, y, z) {
       //  alert("b");
        // isGood = false;
         if (z == "Unauthorized") {
         }
     })
     .complete(function (a, b, c) {
        // alert("c");
     });
    return isGood;
}

//INSERT
function PostData(ControllerName, dataJson) {
    //FORMAT dataJason:  must be an object 
    var isGood = true;
    urlString = wapiService + ControllerName;
  // alert(urlString);
    $.ajax({
        type: "POST",
        url: urlString,
        data: dataJson,
        dataType: 'json',
        contentType: "application/json",
        async: false,
        success: function (data, textStatus, xhr) {
        },
        error: function (msg) {
            isGood = false;
            var thisTitle = "POST ERROR WHILE CALLING:  " + urlString;
            BuildError(thisTitle, msg.responseText);

        }
    })
     .fail(function (x, y, z) {
         isGood = false;
         if (z == "Unauthorized") {           
         }
     })
     .complete(function (a, b, c) {
     });
    return isGood;
}

//DELETE
function DeleteData(ControllerName, Parm, id) {
    //FORMAT Parm: packageMapsId=5&vendorId=2
    // or  where Parm == "id", the method will use the value in the id field as the value passed.
    var isGood = true;

    urlString = wapiService + ControllerName;

    if (Parm == "id") {
        urlString += "/" + id;
    }
    else {
        urlString += "?" + Parm;
    }
   
    var parm = "";
    $.ajax({
        type: "DELETE",
        url: urlString,
        data: parm,
        async: false,
        success: function (data, textStatus, xhr) {
        },
        error: function (msg) {
            isGood = false;
            //alert("delete error");
            var thisTitle = "DELETE ERROR WHILE CALLING:  " + urlString;
            BuildError(thisTitle, msg.responseText);

        }
    })
     .fail(function (x, y, z) {
         isGood = false;
         if (z == "Unauthorized") {
             //alert("not");
         }
     })
     .complete(function (a, b, c) {
     });
    return isGood;
   
}

//DELETE
function DeleteByClass(ControllerName, dataJson) {
    //FORMAT dataJason:  must be an object 
    var isGood = true;
    urlString = wapiService + ControllerName;
    // alert(urlString);
    $.ajax({
        type: "DELETE",
        url: urlString,
        data: dataJson,
        async: false,
        success: function (data, textStatus, xhr) {
        },
        error: function (msg) {
            isGood = false;
            var thisTitle = "DELETE ERROR WHILE CALLING:  " + urlString;
            BuildError(thisTitle, msg.responseText);

        }
    })
     .fail(function (x, y, z) {
         isGood = false;
         if (z == "Unauthorized") {
         }
     })
     .complete(function (a, b, c) {
     });
    return isGood;
}
function BuildError(title, msgText) {

    showError(title + " " + msgText);
}


