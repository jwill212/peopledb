$(function () {
    $("#dialogPerson").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "explode",
            duration: 400
        },
        open: function () {
            $(this).load("dialog_PersonnelRecord.html?ver=2.0.0.2", peopleObj.LoadDialog);
        },
        minWidth: 560,
        maxWidth: 1000
    });

    $("#dialogLOA").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "explode",
            duration: 300
        },

        minWidth: 400

    });

    $("#dialogTERM").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "explode",
            duration: 300
        },

        minWidth: 400

    });

    // Clickable Dropdown
    $('.click-nav > ul').toggleClass('no-js js');
    $('.click-nav .js ul').hide();
    $('.click-nav .js').click(function (e) {
        $('.click-nav .js ul').slideToggle(200);
        $('.clicker').toggleClass('active');
        e.stopPropagation();
    });
    $(document).click(function () {
        if ($('.click-nav .js ul').is(':visible')) {
            $('.click-nav .js ul', this).slideUp();
            $('.clicker').removeClass('active');
        }
    });

    $(".datePickerInput").datepicker();

    $("#SelectionDiv").mouseenter(function () {
        $(this).animate({ "margin-left": "-2px" }, 200);
    });

    $("#mainContent").mouseenter(function () {
        $("#SelectionDiv").animate({ "margin-left": "-190px" }, 200);
    });

    $(".datePicker").mousedown(function () {

        $(this).datepicker("hide");
    });

    $(".datepicker").hide();


    $("#dialog_version").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "explode",
            duration: 300
        },
        minWidth: 400,
        buttons: { "OK": function () { $(this).dialog('close'); } }, 
        open: function () {
            $.get(wapiService + "Admin/Version").done(function (data) {
                $("#webAPIVersion").html(data);
            });
        }
    });

    $("#dialogBulkEdit").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        },
        minWidth: 400,
        buttons: {
            "Go": function () {
                let selected = $("#agentMasterData tbody").find(":checked");
                $(this).find("#dbe_progress").show();
                let progress = $(this).find("#dbe_progress > progress");
                let newTL = $(this).find("#dbe_dd").val();
                for (let index = 0; index < selected.length; index++) {
                    var me = selected[index];
                    var id = $(me.parentNode.parentNode).attr("pdbid");
                    var area = $(this).find("#dbe_options").val();
                    if (!PostData("Agents/BulkChange?ID=" + id + "&Area=" + area + "&NewVal=" + newTL, null)) {
                        alert("Bulk update failed!");
                        return;
                    }
                    progress.val(index + 1);
                }
                alert("Bulk change complete");
                peopleObj.GetPeopleList("Agents");
                $(this).dialog("close");
            },
            "Cancel": function () {
                $("#dialogBulkEdit").dialog('close');
            }
        },
        open: function () {
            let selectedCount = $("#agentMasterData tbody").find(":checked").length;
            $(this).find("#dbe_spanAgentsSel").html(selectedCount);
            $(this).find("#dbe_progress").hide();
            $(this).find("#dbe_progress > progress").attr("max", selectedCount).val(0);
            var data = GetData("Agents", "Filter=Position:TL AND Active:", 0);
            $("#dbe_options").val($("#dbe_options option:first").val());
            $(this).find("#dbe_dd").html(
                Mustache.to_html("{{#.}}<option value='{{PDBId}}'>{{EmployeeName}}</option>{{/.}}", data)
             );
        },
        close: function(){ ; }
    });
});

function closeCalendar() {
    $(".datepicker").hide();
}
