$verFilePath = "..\PDB_wapi\PDB_wapi\Properties\AssemblyInfo.cs"
$verStr = Get-Content $verFilePath
$match = [regex]::Match($verStr,'AssemblyVersion\(\"(\d+\.\d+\.\d+\.)(\d+)"\)')
$buildVer = [int]$match.Groups[2].Value
$buildVer = $buildVer + 1
$newVer = $match.Groups[1].Value + [string]$buildVer
Write-Host "New version: $newVer" -fore:green
(Get-Content $verFilePath) |
	Foreach-Object { $_ -replace '\d+\.\d+\.\d+\.\d+', $newVer } |
	Set-Content $verFilePath

'------ Scan every file for a version match ---------------'
$fileList = Get-ChildItem -Include "*.js","*.html","*.htm" -Exclude "jquery*","mustache*","boot*","*.min.js", "*chart*" -recurse
foreach($file in $fileList){
	(Get-Content $file.PSPath) |
	Foreach-Object { $_ -replace '\d+\.\d+\.\d+\.\d+', $newVer } |
	Set-Content $file.PSPath
}

Write-Host "Done!" -fore:green